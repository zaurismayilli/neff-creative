import { BASE_URL } from "@/shared/constant";
import storage from "@/shared/utils/storage";
import { BaseQueryFn } from "@reduxjs/toolkit/dist/query";
import axios, { AxiosError, AxiosRequestConfig } from "axios";
import Swal from "sweetalert2";

const Axios = axios.create({
	baseURL: BASE_URL,
	headers: {},
});

Axios.interceptors.request.use(
	(config) => {
		const { accessToken, resreshToken } = storage.getAuthToken();
		config.headers.Authorization = accessToken && `Bearer ${accessToken}`;
		config.headers["X-REFRESH-TOKEN"] = resreshToken;
		return config;
	},
	(error) => {
		return Promise.reject(error);
	}
);

Axios.interceptors.response.use(
	(response) => {
		return Promise.resolve(response);
	},
	async (error) => {
		const originalConfig = error.config;
		switch (error.response.status) {
			case 400:
				console.log(error.response.status, error.message, "-------------400-------------");
				return Promise.reject(error);
			case 401:
				console.log(error.response.status, error.message, "-------------401-------------");
				storage.clearAuthToken();
				return Promise.reject(error);
			case 403:
				console.log(error.response.status, error.message, "-------------403-------------");
				storage.clearAuthToken();
				return Promise.reject(error);
			case 406:
				if (!originalConfig._retry) {
					originalConfig._retry = true;
					console.log(error.response.status, error.message, "-------------406-------------");

					const { data } = await Axios.patch("/auth/token/refresh", {
						refreshToken: storage.getAuthToken().resreshToken,
					});

					if (data) storage.saveAuthToken(data);

					originalConfig.headers.Authorization = `Bearer ${storage.getAuthToken().accessToken}`;
					return Axios(originalConfig);
				}
			default:
				return Promise.reject(error);
		}
	}
);

const axiosBaseQuery =
	(): BaseQueryFn<AxiosRequestConfig, unknown, unknown> =>
	async (props): Promise<any> => {
		try {
			const data = await Axios(props);
			return Promise.resolve(data);
		} catch (axiosError) {
			const { response, code } = axiosError as AxiosError | any;

			Swal.fire({
				title: code,
				text: response.data.message,
				icon: "error",
				confirmButtonText: "Go back",
			});
			return Promise.reject(axiosError);
		}
	};

export default axiosBaseQuery;
