import Swal from 'sweetalert2'
import React, { Fragment, useEffect, useRef } from 'react'
import PageCover from '@/shared/Components/PageCover'
import { authHooks } from '@/domain/auth/authApi'
import Loading from '@/shared/Components/Loading'
import { imageParse } from '@/shared/utils/image'
import { SubmitHandler, useForm } from 'react-hook-form'

const Profile = () => {
  const { register: profileRegister, handleSubmit: profileHandleSubmit, setValue: setProfileValue, formState: { errors: profileErrors } } = useForm();
  const { register: passwordRegister, handleSubmit: passwordHandleSubmit, reset: passwordReset, formState: { errors: passwordErrors } } = useForm();
  const { data: userProfile, isLoading: isUserLoading, error, isError: isUserError } = authHooks.useGetMe({})

  const [updateProfile] = authHooks.useUpdateProfile({})
  const [updatePassword] = authHooks.useUpdatePassword({})

  const imageRef = useRef<HTMLElement | any>(null);

  const handleProfile: SubmitHandler<any> = async (body) => {
    await updateProfile(body)
    return Swal.fire({
      title: 'Profil',
      text: "Profil uğurla dəyişildi",
      icon: "success",
      confirmButtonText: "Next"
    });
  }

  const handlePassword: SubmitHandler<any> = async (body) => {
    await updatePassword(body)
    Swal.fire({
      title: 'Şifrə',
      text: "Şifrə uğurla dəyişildi",
      icon: "success",
      confirmButtonText: "Next"
    });
    return passwordReset()
  }

  const onChangeAvatar = async (e: any) => {
    const file = e.target.files[0]
    const formdata = new FormData();
    formdata.append("profile", file)
    await updateProfile(formdata)
  }

  useEffect(() => {
    setProfileValue("firstname", userProfile?.firstname)
    setProfileValue("lastname", userProfile?.lastname)
  }, [userProfile])

  if (isUserLoading) return <Loading />

  if (isUserError) return <Loading />;

  const { image: profileImage, } = userProfile

  const image = imageParse(profileImage)

  return (
    <Fragment>
      <PageCover title="Profile Page" description="" />
      <section>
        <div className="container-fluid">
          <div className="row">
            <div className="profile-block-section">
              <div className="col-md-3 col-sm-3 col-xs-12">
                <div className="profile-block">
                  <div className="profile-cover">
                    <img src="/src/assets/image/slider/image1.jpg" alt="slider" />
                    <div className="profile-img-file">
                      <input type="file" />
                      <button type="button" className="waves-effect"> <i className="fa fa-upload" aria-hidden="true"></i> </button>
                    </div>
                    <div className="profile-image">
                      <div className="profile-img-file">
                        <input ref={imageRef} onChange={onChangeAvatar} type="file" />
                        <button type="button" onClick={() => imageRef.current.click()} className="waves-effect"> <i className="fa fa-upload" aria-hidden="true"></i> </button>
                      </div>
                      <img src={image.path} title={userProfile.email} alt={userProfile.email} />
                    </div>
                  </div>
                  <div className="profile-information">
                    <h4> {userProfile.firstname}-{userProfile.lastname}</h4>
                    <span> Front End Developer *** Freelancer </span>
                  </div>
                  <div className="profile-footer">
                    <a href="#set" className="waves-effect"> Follow me </a>
                  </div>
                </div>
              </div>
              <div className="col-md-9 col-sm-9 col-xs-12">
                <div className="profile-forms">
                  <h2>Profile</h2> <br />
                  <form onSubmit={profileHandleSubmit(handleProfile)} encType={"multipart/form-data"} >
                    <div className="col-md-6">
                      <div className="default-element">
                        <input {...profileRegister("firstname", { required: true })} type="text" className="form-control" placeholder="Firstname" />
                        {profileErrors.firstname && <span>This field is required</span>}
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="default-element">
                        <input {...profileRegister("lastname", { required: true })} type="text" className="form-control" placeholder="Lastname" />
                        {profileErrors.lastname && <span>This field is required</span>}
                      </div>
                    </div>
                    <div className="col-md-12 text-right">
                      <button className="link-md blue waves-effect" type="submit">  Submit </button>
                    </div>
                    <div className="clearfix"></div>
                  </form>
                </div>
                <br />
                <div className="profile-forms">
                  <h2>Password</h2> <br />
                  <form onSubmit={passwordHandleSubmit(handlePassword)}>
                    <div className="col-md-4">
                      <div className="default-element">
                        <input {...passwordRegister("password_old", { required: true })} type="password" className="form-control" placeholder="Password Old" />
                        {passwordErrors.password_old && <span>This field is required</span>}
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="default-element">
                        <input {...passwordRegister("password_new", { required: true })} type="password" className="form-control" placeholder="Password New" />
                        {passwordErrors.password_new && <span>This field is required</span>}
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="default-element">
                        <input {...passwordRegister("password_new_repeat", { required: true })} type="password" className="form-control" placeholder="Password New Repeat" />
                        {passwordErrors.password_new_repeat && <span>This field is required</span>}
                      </div>
                    </div>
                    <div className="col-md-12 text-right">
                      <button className="link-md blue waves-effect" type="submit">  Submit </button>
                    </div>
                    <div className="clearfix"></div>
                  </form>
                </div>
              </div>
              <div className="clearfix"></div>
            </div>
          </div>
        </div>
      </section>
    </Fragment>
  )
}

export default Profile