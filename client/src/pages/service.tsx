import { Fragment } from 'react'
import PageCover from '@/shared/Components/PageCover'
import { serviceHooks } from '@/domain/service/serviceApi'
import ServiceList from '@/shared/Components/Service/list'
import Loading from '@/shared/Components/Loading'

const Service = () => {
  const { data: serviceList, isLoading: isServiceLoading } = serviceHooks.useService({})

  if (isServiceLoading) return <Loading />

  return (
    <Fragment>
      <PageCover title="Service Page" description="it is your Service  page" />
      <section className="section-default company-section">
        <div className="container">
          <div className="row">
            <div className="section-header">
              <h4> Our company </h4>
            </div>
            <ServiceList list={serviceList} />
          </div>
        </div>
      </section>
    </Fragment>
  )
}

export default Service