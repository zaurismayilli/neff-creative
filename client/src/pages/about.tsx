import { Fragment } from 'react'
import PageCover from '@/shared/Components/PageCover'
import { aboutHooks } from '@/domain/about/aboutApi'
import Loading from '@/shared/Components/Loading'

const About = () => {
  const { data: aboutData, isLoading } = aboutHooks.useAbout({})

  if (isLoading) return <Loading />

  const { title, content } = aboutData

  return < Fragment >
    <PageCover title="About us" description="it is your about page" />
    <section>
      <div className="container">
        <div className="row">
          <div className="blue-jumbotron default-jumbotron ">
            <div className="j3-header">
              <h3> {title}  </h3>
            </div>
            <div className="j3-content">
              <p> {content} </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  </Fragment >
}

export default About