import { authHooks } from "@/domain/auth/authApi";
import { useAuth } from "@/shared/hooks";
import { IResponse } from "@/shared/model";
import { SubmitHandler, useForm } from "react-hook-form";

const Register = () => {
  const { register, handleSubmit, formState: { errors } } = useForm();
  const [signIn] = authHooks.useSignUp({})
  const [, setUserToken] = useAuth();

  const onSubmit: SubmitHandler<any> = async (body) => {
    const { data }: IResponse = await signIn(body)
    if (data) setUserToken(data)
  }

  return (
    <section className="default-section">
      <div className="container">
        <div className="row">
          <div className="block-header">
            <h3> Register Us </h3>
          </div>
          <div className="block-content">
            <div className="col-md-10 margin-auto">
              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="col-md-12">
                  <div className="default-element">
                    <input  {...register("firstname", { required: true })} type="text" className="form-control" placeholder="Firstname" />
                    {errors.firstname && <span>This field is required</span>}
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="default-element">
                    <input {...register("lastname", { required: true })} type="text" className="form-control" placeholder="Lastname" />
                    {errors.lastname && <span>This field is required</span>}
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="default-element">
                    <input {...register("email", { required: true })} type="email" className="form-control" placeholder="Email" />
                    {errors.email && <span>This field is required</span>}
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="default-element">
                    <input {...register("password", { required: true })} type="password" className="form-control" placeholder="Password" />
                    {errors.password && <span>This field is required</span>}
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="default-element">
                    <input {...register("password_repeat", { required: true })} type="password" className="form-control" placeholder="Repeat Password" />
                    {errors.password_repeat && <span>This field is required</span>}
                  </div>
                </div>
                <div className="default-element text-center">
                  <button type="submit" className="blue waves-effect"> send form </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Register