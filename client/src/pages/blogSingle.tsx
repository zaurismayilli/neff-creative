import { useParams } from 'react-router-dom'
import { blogHooks } from '@/domain/blog/blogApi'
import { categoryHooks } from '@/domain/category/categoryApi'
import BlogSingleComponent from '@/shared/Components/Blog/Single'
import { getCategoryName } from '@/shared/utils/category'
import { imageParse } from '@/shared/utils/image'
import Loading from '@/shared/Components/Loading'

const BlogSingle = () => {
  const { id } = useParams()
  const { data: blogData, isLoading: isBlogLoading } = blogHooks.useBlogById({ id })
  const { data: categoryData, isLoading: isCategoryLoading } = categoryHooks.useCategory({});

  if (isBlogLoading || isCategoryLoading) return <Loading />

  const { image: blogImage, categoryId, ...rest } = blogData

  const category = getCategoryName(categoryId, categoryData)

  const image = imageParse(blogImage)

  return <BlogSingleComponent {...{ image, category }} {...rest} />
}

export default BlogSingle