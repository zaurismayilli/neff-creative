import { Fragment } from 'react'
import PageCover from '@/shared/Components/PageCover'
import { useSearchParams } from 'react-router-dom';
import { galleryHooks } from '@/domain/gallery/galleryApi';
import { categoryHooks } from '@/domain/category/categoryApi';
import { imageParse } from '@/shared/utils/image';
import { ICategory, IGallery } from '@/shared/model';
import GalleryCategoryItem from '@/shared/Components/Gallery/Item';
import GalleryList from '@/shared/Components/Gallery/List';
import { minMaxNumber } from '@/shared/utils/minMaxNumber';
import { imagesClass } from '@/shared/constant';
import Loading from '@/shared/Components/Loading';

const Gallery = () => {
  const [searchParams] = useSearchParams();
  const categoryId = searchParams.get('categoryId') || null;

  const { data: galleryData, isLoading: isGalleryLoading } = galleryHooks.useGallery({ categoryId })
  const { data: categoryData, isLoading: isCategoryLoading } = categoryHooks.useCategory({});

  if (isGalleryLoading || isCategoryLoading) return <Loading />

  const categoryList = categoryData.map(({ _id, category_name }: ICategory) =>
    <GalleryCategoryItem
      key={_id}
      {...{
        category_name,
        _id
      }} />
  )

  const galleryList = galleryData.gallery.map(({ image: blogImage, ...rest }: IGallery) => {
    const cls = imagesClass[minMaxNumber()]
    const image = imageParse(blogImage)
    return <GalleryList
      {...rest} cls={cls} image={image} />
  })

  return (
    <Fragment>
      <PageCover title="Gallery " description="it is your Gallery page" />
      <section className="section-default blog-section">
        <div className="container">
          <div className="section-header">
            <h4> LATEST Gallery </h4>
          </div>
          {categoryId ?
            <div className='gallery' >
              {galleryList.length ? galleryList :
                <span
                  className="alert alert-danger"
                >
                  no images
                </span>}
            </div>
            :
            <div className="row">

              <div className="blog-template">
                {categoryList}
              </div>
              <div className="clearfix"></div>
            </div>
          }
        </div>
      </section>
    </Fragment>
  )
}

export default Gallery