import { Link } from "react-router-dom";
import { blogHooks } from "@/domain/blog/blogApi";
import BlogItem from "@/shared/Components/Blog/Item";
import { IBlog } from "@/shared/model";
import { categoryHooks } from "@/domain/category/categoryApi";
import { getCategoryName } from "@/shared/utils/category";

import { serviceHooks } from "@/domain/service/serviceApi";
import { imageParse } from "@/shared/utils/image";
import ServiceList from "@/shared/Components/Service/list";
import { SubmitHandler, useForm } from "react-hook-form";
import { subscribeHooks } from "@/domain/subscribe/subscribeApi";
import Swal from "sweetalert2";
import Loading from "@/shared/Components/Loading";



const Home = () => {
  const { register, handleSubmit, reset, formState: { errors } } = useForm();
  const [addSubscribe] = subscribeHooks.addSubscribe({});
  const { data: blogData, isLoading: isBlogLoading } = blogHooks.useBlog({})
  const { data: categoryData, isLoading: isCategoryLoading } = categoryHooks.useCategory({});
  const { data: serviceList, isLoading: isServiceLoading } = serviceHooks.useService({})

  if (isBlogLoading || isCategoryLoading || isServiceLoading) return <Loading />

  const blogList = blogData?.blog.map((
    {
      categoryId,
      image: blogImage,
      ...rest }: IBlog) => {
    const category = getCategoryName(categoryId, categoryData)
    const image = imageParse(blogImage)
    return <BlogItem
      key={rest._id}
      {...rest}
      {...{
        category,
        image,
      }} />
  })

  const onSubmit: SubmitHandler<any> = async (body) => {
    await addSubscribe(body)
    Swal.fire({
      title: 'Mail',
      text: "Mesajınız uğurla əlavə edildi",
      icon: "success",
      confirmButtonText: "Next"
    });
    return reset()
  }

  return (
    <>
      <section className="slider-section">
        <div className="container-fluid">
          <div className="row">
            <div className="slider-content-block">
              <div className="slider-text">
                <h1> WEB DESIGN & ECOMMERCE DEVELOPMENT <br />  FOR BUSINESSES & AGENCIES </h1>
              </div>
              <div className="slider-link">
                <Link className="link-md blue" to="#">  Start a project </Link>
                <Link className="link-md white" to="#">View our work </Link>
              </div>
            </div>
            <div className="slider-cover">
              <img src="assets/image/slider/image8.jpg" alt="slider " />
            </div>
            <div className="slider-video">
              <video muted loop autoPlay>
                <source src="/src/assets/video/video.mp4" type="video/mp4" />
                Your browser does not support HTML5 video.
              </video>
            </div>
          </div>
        </div>
      </section>

      <section className="section-default blog-section">
        <div className="container">
          <div className="row">
            <div className="section-header">
              <h4> LATEST NEWS </h4>
            </div>
            <div className="blog-template">
              {blogList}
            </div>
          </div>
        </div>
      </section>

      <section className="section-default company-section">
        <div className="container">
          <div className="row">
            <div className="section-header">
              <h4> Our company </h4>
            </div>
            <ServiceList list={serviceList} />
          </div>
        </div>
      </section>
      <section className="section-default contact-section">
        <div className="container">
          <div className="row">
            <div className="col-md-6 col-sm-10 col-xs-12 margin-auto">
              <div className="form-block">
                <form onSubmit={handleSubmit(onSubmit)}>
                  <div className="col-md-6 col-xs-6">
                    <div className="default-element">
                      <input {...register("name", { required: true })} type="text" className="form-control" placeholder="Name" />
                      {errors.name && <span style={{ color: "#fff", }} >This field is required</span>}
                    </div>
                  </div>
                  <div className="col-md-6 col-xs-6">
                    <div className="default-element">
                      <input {...register("email", { required: true })} type="email" className="form-control" placeholder="Email" />
                      {errors.email && <span style={{ color: "#fff" }} >This field is required</span>}
                    </div>
                  </div>
                  <div className="clearfix"></div>
                  <div className="col-md-12">
                    <div className="default-element">
                      <input {...register("subject", { required: true })} type="text" className="form-control" placeholder="Subject" />
                      {errors.subject && <span style={{ color: "#fff" }} >This field is required</span>}
                    </div>
                  </div>
                  <div className="col-md-12">
                    <div className="default-element">
                      <textarea {...register("text", { required: true })} className="form-control" placeholder="Text" ></textarea>
                      {errors.text && <span style={{ color: "#fff" }} >This field is required</span>}
                    </div>
                  </div>
                  <div className="col-md-12">
                    <div className="default-element text-right">
                      <button type="submit" className="blue"> send form </button>
                    </div>
                  </div>
                  <div className="clearfix"></div>
                </form>
              </div>
            </div>
            <div className="clearfix"></div>
          </div>
        </div>
      </section>
    </>
  )
}

export default Home;
