import { Fragment } from 'react'
import PageCover from '@/shared/Components/PageCover'
import { contactHooks } from '@/domain/contact/contactApi'
import Map from '@/shared/Components/Map'
import Loading from '@/shared/Components/Loading'

const Contact = () => {
  const { data: contactData, isLoading } = contactHooks.useContact({})

  if (isLoading) return <Loading />

  const {
    title,
    address,
    content,
    description,
    email,
    mobile,
    position
  } = contactData
  return (
    <Fragment>
      <PageCover title="Contact " description="it is your Contact  page" />
      <section className="section-default company-section">
        <div className="container">
          <div className="row">
            <div className="section-header">
              <h4> Contact us </h4>
            </div>
            <div className="col-md-4">
              <table className="mini-table">
                <tbody>
                  <tr>
                    <th> title :  </th>
                    <td> {title}  </td>
                  </tr>
                  <tr>
                    <th> address :  </th>
                    <td> {address}  </td>
                  </tr>
                  <tr>
                    <th> email :  </th>
                    <td> <a href={`mailto:${email}`} > {email} </a> </td>
                  </tr>
                  <tr>
                    <th> mobile :  </th>
                    <td> <a href={`tel:${mobile}`} >  {mobile}  </a> </td>
                  </tr>
                  <tr>
                    <th> mini about us :  </th>
                    <td>
                      {description}
                    </td>
                  </tr>
                  <tr>
                    <th> content :  </th>
                    <td>
                      {content}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="col-md-8">
              <Map lat={position.latitude} lng={position.longitude} />
            </div>
          </div>
        </div>
      </section>
    </Fragment>
  )
}

export default Contact