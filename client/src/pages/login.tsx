import { SubmitHandler, useForm } from "react-hook-form";
import { authHooks } from "@/domain/auth/authApi";
import { IResponse } from "@/shared/model";
import { useAuth } from "@/shared/hooks";
import { Link } from "react-router-dom";

const Login = () => {
  const { register, handleSubmit, formState: { errors } } = useForm();
  const [signIn] = authHooks.useSignIn({})
  const [, setUserToken] = useAuth();

  const onSubmit: SubmitHandler<any> = async (body) => {
    const { data }: IResponse = await signIn(body)
    if (data) setUserToken(data)
  }

  return (
    <section className="default-section login-section">
      <div className="container">
        <div className="row">
          <div className="block-header">
            <h3> Login Us</h3>
          </div>
          <div className="block-content">
            <div className="col-md-6 margin-auto">
              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="col-md-12">
                  <div className="default-element">
                    <input type="email" {...register("email", { required: true })} className="form-control" placeholder="Email" />
                    {errors.email && <span>This field is required</span>}
                  </div>
                </div>
                <div className="col-md-12">
                  <div className="default-element">
                    <input type="password" autoComplete="off" {...register("password", { required: true })} className="form-control" placeholder="Password" />
                    {errors.password && <span>This field is required</span>}
                  </div>
                </div>
                <div className="default-element text-center">
                  <button type="submit" className="blue waves-effect"> send form </button>
                </div>
                Aren't user? : <Link to={'/auth/signup'}> Sign up </Link>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Login