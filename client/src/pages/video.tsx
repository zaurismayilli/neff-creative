import { Fragment } from 'react'
import PageCover from '@/shared/Components/PageCover'
import Pagination from '@/shared/Components/Pagination'
import VideoItem from '@/shared/Components/Video/Item'
import { IVideo } from '@/shared/model'
import { useSearchParams } from 'react-router-dom'
import { videoHooks } from '@/domain/video/videoApi'
import { categoryHooks } from '@/domain/category/categoryApi'
import { getCategoryName } from '@/shared/utils/category'
import { imageParse } from '@/shared/utils/image'
import Loading from '@/shared/Components/Loading'

const Video = () => {
  const [searchParams] = useSearchParams();
  const page = +(searchParams.get('page') ?? 1);
  const limit = +(searchParams.get('limit') ?? 3);

  const { data: videoData, isLoading: isVideoLoading } = videoHooks.useVideo({ page, limit })
  const { data: categoryData, isLoading: isCategoryLoading } = categoryHooks.useCategory({});

  if (isVideoLoading || isCategoryLoading) return <Loading />

  const videoList = videoData.video.map(({ cover: videoCover, _id, categoryId, ...rest }: IVideo) => {
    const category = getCategoryName(categoryId, categoryData)
    const cover = imageParse(videoCover)
    return <VideoItem
      key={_id}
      {...{
        category,
        cover,
        _id
      }}
      {...rest}
    />
  })

  return (
    <Fragment>
      <PageCover title="Video Page" description="it is your Video page" />
      <section className="section-default">
        <div className="container">
          <div className="row">
            <div className="section-header">
              <h4> LATEST Video </h4>
            </div>
            <div className="blog-template">
              {videoList}
              <div className="clearfix"></div>
              <Pagination
                basePath={"video"}
                {...{ limit }}
                count={Math.ceil(videoData.count / limit)}
                currentPage={1}
              />
            </div>
          </div>
        </div>
      </section>
    </Fragment>
  )
}

export default Video