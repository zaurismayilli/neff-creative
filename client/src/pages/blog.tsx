import { useSearchParams } from 'react-router-dom'
import PageCover from '@/shared/Components/PageCover'
import Pagination from '@/shared/Components/Pagination'
import BlogItem from '../shared/Components/Blog/Item'
import { IBlog } from '@/shared/model'
import { blogHooks } from '@/domain/blog/blogApi'
import { categoryHooks } from '@/domain/category/categoryApi'
import { getCategoryName } from '@/shared/utils/category'
import { imageParse } from '@/shared/utils/image'
import Loading from '@/shared/Components/Loading'

const Blog = () => {
  const [searchParams] = useSearchParams();
  const page = +(searchParams.get('page') ?? 1);
  const limit = +(searchParams.get('limit') ?? 3);

  const { data: blogData, isLoading: isBlogLoading } = blogHooks.useBlog({ page, limit })
  const { data: categoryData, isLoading: isCategoryLoading } = categoryHooks.useCategory({});

  if (isBlogLoading || isCategoryLoading) return <Loading />

  const blogList = blogData.blog.map(({ title, author, categoryId, content, created_at, image: blogImage, _id }: IBlog) => {
    const category = getCategoryName(categoryId, categoryData)
    const image = imageParse(blogImage)

    return <BlogItem
      key={_id}
      {...{
        title,
        author,
        content,
        created_at,
        category, image,
        _id
      }} />
  })

  return <>
    <PageCover title="Blog" description="it is your blog page" />
    <section className="section-default blog-section">
      <div className="container">
        <div className="row">
          <div className="section-header">
            <h4> LATEST NEWS </h4>
          </div>
          <div className="blog-template">
            {blogList}
          </div>
          <div className="clearfix"></div>
          <Pagination
            basePath={"blog"}
            {...{ limit }}
            count={Math.ceil(blogData.count / limit)}
            currentPage={1}
          />
        </div>
      </div>
    </section>
  </>
}

export default Blog