import { Fragment } from 'react'
import { useParams } from 'react-router-dom'
import { videoHooks } from '@/domain/video/videoApi'
import { categoryHooks } from '@/domain/category/categoryApi'
import { getCategoryName } from '@/shared/utils/category'
import VideoSingleComponent from '@/shared/Components/Video/Single'
import Loading from '@/shared/Components/Loading'

const VideoSingle = () => {
  const { id } = useParams()
  const { data: videoData, isLoading: isvideoLoading } = videoHooks.useVideoById({ id })
  const { data: categoryData, isLoading: isCategoryLoading } = categoryHooks.useCategory({});

  if (isvideoLoading || isCategoryLoading) return <Loading />

  const { categoryId, ...rest } = videoData

  const category = getCategoryName(categoryId, categoryData)

  return (
    <Fragment>
      <VideoSingleComponent
        {...{ category }} {...rest}
      />
    </Fragment>
  )
}

export default VideoSingle