import { ReactElement, useEffect } from 'react'
import Header from "@/shared/Components/Header"
import Footer from "@/shared/Components/Footer"
import script from '@/assets/script/index.js'

const Layout = ({ children }: { children: ReactElement }) => {

  useEffect(() => {
    script()
  }, [])

  return (
    <>
      <Header />
      <div className='body' >{children}</div>
      <Footer />
    </>
  )
}

export default Layout
