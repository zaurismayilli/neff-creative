export const BASE_URL = "http://localhost:1001/api/";

export const menuList = [
	{
		title: "Home",
		path: "/home",
	},
	{
		title: "About",
		path: "/about",
	},
	{
		title: "Blog",
		path: "/blog",
		nested: [
			{
				title: "Gallery",
				path: "/gallery",
			},
			{
				title: "Gallery",
				path: "/gallery",
			},
		],
	},
	{
		title: "Contact",
		path: "/contact",
	},
	{
		title: "Gallery",
		path: "/gallery",
	},
	{
		title: "Service",
		path: "/service",
	},
	{
		title: "Video",
		path: "/video",
	},
];

export const socialList = [
	{
		path: "https://www.facebook.com/",
		icon: "facebook",
		className: "fb",
	},
	{
		path: "https://twitter.com",
		icon: "twitter",
		className: "tw",
	},
	{
		path: "https://www.google.az",
		icon: "google",
		className: "go",
	},
	{
		path: "https://www.instagram.com/",
		icon: "instagram",
		className: "in",
	},
	{
		path: "https://www.pinterest.com",
		icon: "pinterest",
		className: "pn",
	},
];

export const mapStyle = [
	{ featureType: "all", elementType: "labels.text.fill", stylers: [{ color: "#ffffff" }] },
	{
		featureType: "all",
		elementType: "labels.text.stroke",
		stylers: [{ color: "#000000" }, { lightness: 13 }],
	},
	{ featureType: "administrative", elementType: "geometry.fill", stylers: [{ color: "#000000" }] },
	{
		featureType: "administrative",
		elementType: "geometry.stroke",
		stylers: [{ color: "#144b53" }, { lightness: 14 }, { weight: 1.4 }],
	},
	{ featureType: "landscape", elementType: "all", stylers: [{ color: "#08304b" }] },
	{
		featureType: "poi",
		elementType: "geometry",
		stylers: [{ color: "#0c4152" }, { lightness: 5 }],
	},
	{ featureType: "road.highway", elementType: "geometry.fill", stylers: [{ color: "#000000" }] },
	{
		featureType: "road.highway",
		elementType: "geometry.stroke",
		stylers: [{ color: "#0b434f" }, { lightness: 25 }],
	},
	{ featureType: "road.arterial", elementType: "geometry.fill", stylers: [{ color: "#000000" }] },
	{
		featureType: "road.arterial",
		elementType: "geometry.stroke",
		stylers: [{ color: "#0b3d51" }, { lightness: 16 }],
	},
	{ featureType: "road.local", elementType: "geometry", stylers: [{ color: "#000000" }] },
	{ featureType: "transit", elementType: "all", stylers: [{ color: "#146474" }] },
	{ featureType: "water", elementType: "all", stylers: [{ color: "#021019" }] },
];
export const GOOGLE_MAP_KEY = "AIzaSyD79lfijXfmSZxeSDuH35UZqunZtSJb6NI";

export const imagesWidth = [950, 850, 950, 970, 570, 780, 650, 990, 995, 999];
export const imagesClass = [
	"horizontal-s",
	"horizontal-m",
	"horizontal-l",
	"vertical-s",
	"vertical-m",
	"vertical-l",
	"big-s",
	"big-m",
	"big-l",
	"light",
];
