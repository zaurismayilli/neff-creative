import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import _ from "lodash";

import storage from "../utils/storage";
import { IAuthSignIn } from "../model";

const protectedRoutes = ["profile"];

export const useAuth = () => {
	const [isLoggedIn, setIsLoggedIn] = useState(false);
	const navigate = useNavigate();
	const { pathname } = useLocation();

	const checkUserToken = () => {
		if (!!storage.getAuthToken().accessToken) {
			return setIsLoggedIn(true);
		}

		setIsLoggedIn(false);
		return protectedRoutes.map((item) => {
			return !!pathname.includes(item) && navigate("/auth/signin");
		});
	};

	const setUserToken = (token: IAuthSignIn) => {
		storage.saveAuthToken(token);
		return navigate("/auth/profile");
	};

	useEffect(() => {
		checkUserToken();
	}, [isLoggedIn, pathname]);

	return [isLoggedIn, setUserToken] as const;
};
