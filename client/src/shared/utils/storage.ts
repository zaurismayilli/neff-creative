import { IAuthSignIn } from "../model";

class Storage {
	private accessKey = "accessToken";
	private refreshKey = "refreshToken";

	saveAuthToken({ accessToken, refreshToken }: IAuthSignIn) {
		localStorage.setItem(this.accessKey, accessToken);
		localStorage.setItem(this.refreshKey, refreshToken);
	}

	getAuthToken() {
		return {
			accessToken: localStorage.getItem(this.accessKey),
			resreshToken: localStorage.getItem(this.refreshKey),
		};
	}

	clearAuthToken() {
		localStorage.removeItem(this.accessKey);
		localStorage.removeItem(this.refreshKey);
	}
}

const storage = new Storage();

export default storage;
