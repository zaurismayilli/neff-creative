export const getCategoryName = (id: string | unknown, category: any) => {
	return category.find(({ _id }: any) => _id === id)?.category_name;
};
