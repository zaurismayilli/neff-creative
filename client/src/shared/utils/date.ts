const date = {
	display: (date: any) => {
		const modifiedDate = new Date(date);

		return {
			DDMMYYYY: modifiedDate.toLocaleDateString("pt-PT"),
			YYYYMMDD: modifiedDate.toLocaleDateString("en-CA"),
		};
	},
};

export default date;
