export interface IMenu {
	title: string;
	path: string;
	nested?: Array<IMenu>;
}

export enum EBlogCategory {
	GAME = "game",
	MOVIE = "movie",
}

export interface IFile {
	filename?: string;
	directory?: string;
	path: string;
}

export interface IBlog {
	_id?: string;
	title: string;
	author: string;
	email?: string;
	created_at: string;
	image: IFile;
	content: string;
	categoryId?: string;
	category?: string;
}

export interface IGallery {
	_id: string;
	title: string;
	categoryId?: string;
	created_at: string;
	image: IFile;
	cls?: string;
}

export interface ICategory {
	_id?: string;
	category_name: string;
}

export interface IVideo {
	_id?: string;
	title: string;
	author: string;
	embedId?: string;
	cover: IFile;
	description: string;
	created_at: string;
	categoryId?: string;
	category?: string;
}

export interface IAuthSignIn {
	accessToken: string;
	refreshToken: string;
}

export interface IService {
	_id?: string;
	title: string;
	image: IFile;
}

export interface IResponse {
	data?: IAuthSignIn;
	error?: any;
}
