import { Link } from "react-router-dom";
import { menuList } from "@/shared/constant";
import { IMenu } from "@/shared/model";

import AuthHeader from "../AuthHeader";
import { useState } from "react";

const Header = () => {
  const [searchInputValue, setSearchInputValue] = useState<string>('')
  const menuMap = (list: IMenu[]) => {
    return list.map(({ title, path, nested }: IMenu, key) => {
      return (
        <li onClick={() => window.scrollTo(0, 0)} {...{ key }}  >
          {nested ? <> <Link to={path} className="dropdown-m" > {title} </Link> <ul className='header--menu' >{menuMap(nested)}</ul>  </> :
            <Link to={path}>{title}</Link>}
        </li>
      )
    })
  }

  const onSubmit = () => {
    return window.open("//" + "google.com/search?q=" + searchInputValue, '_blank');
  }

  return (
    <>
      {/* ==== ==== ==== ==== BANNER  start ==== ==== ==== ==== */}
      <section className="banner-section" id="banner-section">
        <div className="container-fluid">
          <Link to="#">
            New Resident Evil 2 Remake Release PRE ORDER NOW :{" "}
            <span className="waves-effect"> BUY </span>{" "}
            <i className="icon-arrow-right white"></i>{" "}
          </Link>
          <button type="button" id="banner-remove">
            <i className="icon-cancel white"></i>{" "}
          </button>
        </div>
      </section>
      <header id="header">
        <div className="container">
          <div className="row">
            <nav>
              <div className="header-left">
                <div className="logo">
                  <Link to="/" onClick={() => window.scrollTo(0, 0)}>
                    <img src="/src/assets/css/icons/logo.png" alt="Neff Creative" />
                  </Link>
                </div>
              </div>
              <div className="header-right">
                <div className="menu">
                  <ul className="menus">
                    {menuMap(menuList)}
                    <AuthHeader />
                  </ul>
                </div>
                <button type="button" id="mobile-btn" className="mobile-bt">
                  <i className="line"></i>
                  <i className="line"></i>
                  <i className="line"></i>
                  <i className="line"></i>
                  <i className="line"></i>
                  <i className="line"></i>
                </button>
                <div className="search-block">
                  <button type="button" id="search-button" className="default-btn">
                    <i className="icon-search white"></i>
                  </button>
                </div>
              </div>
            </nav>
          </div>
        </div>
        <div id="top-line" className="top-line"></div>
      </header>
      {/*  search block */}
      <div className="search-form" id="search-form">
        <button type="button" className="close-search" id="close-search">
          <i className="icon-cancel white"></i>{" "}
        </button>
        <form onSubmit={onSubmit} >
          <input value={searchInputValue} onChange={(e) => setSearchInputValue(e.target.value)} type="text" className="search-input" placeholder="search" />
          <button type="submit" className="default-btn btn-green white">
            search
          </button>
        </form>
      </div>
      {/* ==== ==== ==== ==== HEADER  end ==== ==== ==== ==== */}
    </>
  );
};

export default Header;
