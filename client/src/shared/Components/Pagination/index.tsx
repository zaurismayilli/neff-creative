import { useState } from 'react'
import { Link } from 'react-router-dom';

import './index.scss'

interface IProps {
  count: number,
  currentPage?: number;
  basePath?: string;
  limit?: number;
}

const Pagination = (
  {
    count = 5,
    currentPage = 1,
    basePath = '/',
    limit = 3
  }: IProps
) => {
  const [current, setCurrent] = useState(currentPage)
  const counts = Array.from({ length: count }, (_, i) => i + 1).map(item => (
    <li
      key={item}
      onClick={() => setCurrent(item)}
      className='pagination--item'
    >
      <Link to={`/${basePath}?page=${item}&limit=${limit}`} >
        {item}
      </Link>
    </li>
  ))

  const prevPage = () => {
    if (current >= 1) setCurrent(current - 1)
  }

  const nextPage = () => {
    if (current < count) setCurrent(current + 1)
  }
  return (
    <div className='paging' >
      <ul className='pagination' >
        <li className={`pagination--item ${current == 1 ? 'pagination--item-disabled' : ''}`} onClick={prevPage} >
          <Link to={`/${basePath}?page=${current - 1}&limit=${limit}`} >
            {'<'}
          </Link>
        </li>
        {counts}
        <li className={`pagination--item ${current == count ? 'pagination--item-disabled' : ''}`} onClick={nextPage} >
          <Link to={`/${basePath}?page=${current + 1}&limit=${limit}`} >
            {'>'}
          </Link>
        </li>
      </ul>
    </div >
  )
}

export default Pagination
