import { Link, useNavigate } from "react-router-dom";
import { authHooks } from "@/domain/auth/authApi";
import { useAuth } from "@/shared/hooks";
import storage from "@/shared/utils/storage";

import Loading from "../Loading";

const ProfileInfo = () => {
  const { data: userProfile, isLoading: isUserLoading, error, isError: isUserError } = authHooks.useGetMe({})
  const [userSignOut] = authHooks.useSignOut()
  const navigate = useNavigate();

  if (isUserLoading) return <Loading />

  if (isUserError) return null

  const signOut = () => {
    userSignOut({})
    storage.clearAuthToken();
    navigate('/')
  }

  return <>
    <span className="dropdown-m">
      {userProfile.firstname}-{userProfile.lastname}
    </span>
    <ul className='header--menu'>
      <li>
        <Link to="/auth/profile" className="waves-effect"  > Profile </Link>
      </li>
      <li>
        <span className="waves-effect" onClick={signOut} > sign out </span>
      </li>

    </ul>
  </>
}

const AuthHeader = () => {
  const [isLoggedIn] = useAuth();

  return <li>
    {isLoggedIn ?
      <ProfileInfo />
      :
      <Link to={'auth/signin'}> Sign in </Link>}
  </li>
}

export default AuthHeader