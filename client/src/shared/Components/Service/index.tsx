import { IService } from "@/shared/model"

const ServiceItem = ({ image, title }: IService) =>
  <li>
    <img src={image.path} alt={title} />
    {title}
  </li>



export default ServiceItem
