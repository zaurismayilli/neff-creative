import { IService } from '@/shared/model'
import { imageParse } from '@/shared/utils/image'
import { Swiper, SwiperSlide } from 'swiper/react'
import SwiperCore, { Autoplay } from 'swiper';

SwiperCore.use([Autoplay]);

import ServiceItem from '.'

import "swiper/css";

interface IProps {
  list: IService[]
}

const ServiceList = ({ list }: IProps) => {
  const serviceList = list?.map(({ _id, image: serviceImage, title }: IService) => {
    const image = imageParse(serviceImage)
    return <SwiperSlide key={_id} > <ServiceItem  {...{ title, image }} /> </SwiperSlide>
  })

  return (
    <ul className="company-slider">
      <Swiper
        slidesPerView={4}
        spaceBetween={0}
        navigation={true}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        className="mySwiper"
      >
        {serviceList}
        {serviceList}
      </Swiper>
    </ul>
  )
}

export default ServiceList
