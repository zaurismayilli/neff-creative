import { useEffect, useRef, useState } from 'react'
import { GoogleMap, InfoWindow, useLoadScript, MarkerF } from '@react-google-maps/api';
import { GOOGLE_MAP_KEY, mapStyle } from '@/shared/constant';

import './index.scss'

interface IProps {
  lat: number;
  lng: number;
  zoom?: number;
}

const Map = ({
  lat,
  lng,
  zoom = 16,
}: IProps) => {
  const mapRef = useRef<HTMLDivElement | null>(null);
  const [width, setWidth] = useState<number>()
  const [height, setHeight] = useState<number>()
  const { isLoaded } = useLoadScript({
    googleMapsApiKey: GOOGLE_MAP_KEY,
  });

  const [center, setCenter] = useState({
    lat,
    lng,
  })

  useEffect(() => {
    setWidth(mapRef.current?.offsetWidth);
    setHeight(mapRef.current?.offsetHeight)
  }, [])

  const containerStyle = {
    width,
    height
  };

  const options = {
    fullscreenControl: false,
    zoomControl: false,
    mapTypeControl: false,
    streetViewControl: false,
    styles: mapStyle,
    isFractionalZoomEnabled: true,
    scaleControl: true,
  }

  return (
    <div className='map' ref={mapRef} >
      {!isLoaded ? 'loading' :
        <GoogleMap
          {...{ zoom, center }}
          mapContainerStyle={containerStyle}
          options={options}
        >
          <InfoWindow
            position={center}
          >
            <div style={{ backgroundColor: '#65e4a3', opacity: 0.75, padding: 12 }}>
              <div style={{ fontSize: 16, color: `#000` }}>
                Hello, World!
              </div>
            </div>
          </InfoWindow>
          <MarkerF
            position={center}
            icon={"http://maps.google.com/mapfiles/ms/icons/green-dot.png"}
          />
        </GoogleMap>
      }
    </div>
  )
}

export default Map