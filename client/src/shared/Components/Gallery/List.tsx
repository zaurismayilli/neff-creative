import { ICategory, IGallery } from "@/shared/model";

import './index.scss'

const GalleryList = (
  {
    _id,
    title,
    categoryId,
    created_at,
    image,
    cls,
  }
    : IGallery) => {
  return (
    <div className={`gallery--image gallery--image-${cls}`} >
      <img src={image.path} alt={title} />
    </div>
  )
}

export default GalleryList
