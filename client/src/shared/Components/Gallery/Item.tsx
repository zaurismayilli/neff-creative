import { ICategory, IGallery } from "@/shared/model";
import { Link } from "react-router-dom";

const GalleryCategoryItem = (
  {
    _id,
    category_name,
  }
    : ICategory) => {
  return (
    <div className="col-md-4 col-sm-6 col-xs-12">
      <Link to={`/gallery?categoryId=${_id}`} >
        <div className="blog-header">
          <h2>  {category_name}  </h2>
        </div>
        <div className="blog-image">
          <img src={"https://cdn.wccftech.com/wp-content/uploads/2023/02/WCCFresidentevil4remake14-728x410.jpg"} alt={category_name} />
        </div>
      </Link>
    </div>
  )
}

export default GalleryCategoryItem
