import { Link } from "react-router-dom";
import { IBlog } from "@/shared/model";
import date from "@/shared/utils/date";

const BlogItem = (
  {
    _id,
    title,
    author,
    created_at,
    image,
    content,
    category
  }
    : IBlog) => (
  <div className="col-md-4 col-sm-6 col-xs-12">
    <Link to={`/blog/${_id}`} >
      <div className="blog-header">
        <h2> {title} </h2>
        <span style={{ textAlign: 'left' }} >
          <code>author:  </code>
          {author}
          <code> <br />
            Posted On :
          </code>
          {date.display(created_at).DDMMYYYY}
        </span>
      </div>
      <div className="blog-image">
        <img src={image.path} alt={title} />
      </div>
      <div className="blog-content">
        <p> {content} </p>
      </div>
      <div className="blog-category">
        <span> Categories :  </span>
        <ul className="blog-category-ul">
          <li>  {category} </li>
        </ul>
      </div>
      <div className="blog-more">
        <button type="button"> continue reading </button>
      </div>
    </Link>
  </div>
)

export default BlogItem