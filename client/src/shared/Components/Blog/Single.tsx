import { IBlog } from '@/shared/model'
import date from '@/shared/utils/date';

import PageCover from '../PageCover'

const BlogSingle = ({ title, author, created_at, image, category, content }: IBlog) =>
  <>
    <PageCover title="Blog Single" description="it is your Blog Single page" />
    <section className="section-default blog-section blog-section-single ">
      <div className="container">
        <div className="row">
          <div className="blog-template">
            <div className="col-md-12">
              <div className="blog-header">
                <h2> {title} </h2>
                <span style={{ textAlign: 'left' }} >
                  <code>author:  </code>
                  {author}
                  <code> <br />
                    Posted On :
                  </code>
                  {date.display(created_at).DDMMYYYY}
                </span>
              </div>
              <div className="blog-image">
                <img src={image.path} alt={title} />
              </div>
              <div className="blog-category">
                <span> Categories :  </span>
                <ul className="blog-category-ul">
                  <li>  {category}  </li>
                </ul>
              </div>
              <div className="blog-content">
                <p>
                  {content}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </>

export default BlogSingle