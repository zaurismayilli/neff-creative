import { Link } from 'react-router-dom'
import React, { Fragment } from 'react'
import { menuList, socialList } from '@/shared/constant'
import { IMenu } from '@/shared/model'

const Footer = () => {
  const menuMap = (list: IMenu[]) => {
    return list.map(({ title, path, nested }: IMenu, key) => {
      return (
        <Fragment key={key} >
          <li key={path} >
            <Link to={path}>{title}</Link>
          </li>
        </Fragment>
      )
    })
  }

  const social = socialList.map(({ className, icon, path }, key) => (
    <li key={key} >
      <Link
        to={path}
        {...{ className }}
      >
        <i className={`fa fa-${icon}`} />
      </Link>
    </li>
  ))

  return (
    <React.Fragment>
      <footer>
        <div className="container">
          <div className="col-md-6 col-sm-12 col-xs-12">
            <div className="inline-flex">
              <div className="f-content">
                <div className="logo">
                  <Link to="/" > <img src="/src/assets/css/icons/logo.png" alt="Neff Creative" /> </Link>
                </div>
                <div className="copyright">
                  <p>1000 North Carolina Music Factory <br /> Blvd Charlotte, NC 28206 <br /> Copyright 2018. All Rights Reserved.</p>
                </div>
              </div>
              <ul className="f-menu">
                {menuMap(menuList)}
              </ul>
            </div>
          </div>
          <div className="col-md-6 col-sm-12 col-xs-12">
            <div className="inline-flex">
              <div className="f-header"> <h4> GET IN TOUCH </h4> </div>
              <div className="f-social">
                <ul>
                  {social}
                </ul>
              </div>
            </div>
            <div className="f-feeds">
              <ul>
                <li>
                  <span> A research-based article on the most important features you need to start your wholesale eCommerce store. Read the…
                    <Link to="https://twitter.com/i/web/status/1075652373521944576">twitter.com/i/web/status/1…</Link>
                  </span>
                  <span>
                    <Link to="http://twitter.com/PSDCenter/status/1075652373521944576" title="view tweet on twitter">about 2 days ago</Link>
                  </span>
                </li>
                <li>
                  <span> A research-based article on the most important features you need to start your wholesale eCommerce store. Read the…
                    <Link to="https://twitter.com/i/web/status/1075652373521944576">twitter.com/i/web/status/1…</Link>
                  </span>
                  <span>
                    <Link to="http://twitter.com/PSDCenter/status/1075652373521944576" title="view tweet on twitter">about 2 days ago</Link>
                  </span>
                </li>
              </ul>
            </div>
          </div>
          <div className="clearfix"></div>
          <div className="mini-cpyright">
            <p> This is designed by Scott Kennedy 2018. All Rights Reserved </p>
          </div>
        </div>
      </footer>
    </React.Fragment>
  )
}

export default Footer