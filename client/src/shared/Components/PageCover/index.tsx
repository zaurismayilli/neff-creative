interface IProps {
  title: string;
  description: string
}

const PageCover = ({ title, description }: IProps) => {
  return (
    <section className="default-section about-section">
      <div className="container">
        <div className="row">
          <div className="block-header">
            <h3> {title} </h3>
          </div>
          <div className="block-content">
            <p>
              {description}
            </p>
          </div>
        </div>
      </div>
    </section>
  )
}

export default PageCover
