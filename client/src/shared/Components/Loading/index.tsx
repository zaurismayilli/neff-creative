const Loading = () => (
  <div className="text-center">
    <div className="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
  </div>
)

export default Loading
