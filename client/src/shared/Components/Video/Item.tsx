import { Link } from "react-router-dom";
import { IVideo } from "@/shared/model";
import date from "@/shared/utils/date";

const VideoItem = (
  {
    _id,
    title,
    author,
    description,
    cover,
    category,
    created_at
  }
    : IVideo) => (
  <div className="col-md-4 col-sm-6 col-xs-12">
    <Link to={`/video/${_id}`} >
      <div className="blog-header">
        <h2> {title} </h2>
        <span style={{ textAlign: 'left' }} >
          <code>author:  </code>
          {author}
          <code> <br />
            Posted On :
          </code>
          {date.display(created_at).DDMMYYYY}
        </span>
      </div>
      <div className="blog-image">
        <img src={cover.path} alt={title} />
      </div>
      <div className="blog-content">
        <p> {description} </p>
      </div>
      <div className="blog-category">
        <span> Categories :  </span>
        <ul className="blog-category-ul">
          <li>  {category} </li>
        </ul>
      </div>
      <div className="blog-more">
        <button type="button"> more </button>
      </div>
    </Link>
  </div>
);

export default VideoItem