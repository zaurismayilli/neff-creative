import { Fragment } from 'react'
import { IVideo } from '@/shared/model'
import date from '@/shared/utils/date'

import PageCover from '../PageCover'
import YoutubeEmbed from '../Youtube'

const VideoSingle = (
  {
    title,
    author,
    created_at,
    embedId,
    category,
    description
  }: IVideo
) => {
  return (
    <Fragment>
      <PageCover title="Video Single" description="it is your Video Single page" />
      <section className="section-default blog-section blog-section-single ">
        <div className="container">
          <div className="row">
            <div className="blog-template">
              <div className="col-md-12">
                <div className="blog-header">
                  <h2> {title} </h2>
                  <span style={{ textAlign: 'left' }} >
                    <code>author:  </code>
                    {author}
                    <code> <br />
                      Posted On :
                    </code>
                    {date.display(created_at).DDMMYYYY}
                  </span>
                </div>
                <div className="blog-image">
                  {embedId && <YoutubeEmbed {...{ embedId, title }} />}
                </div>
                <div className="blog-category">
                  <span> Categories :  </span>
                  <ul className="blog-category-ul">
                    <li>  {category}  </li>
                  </ul>
                </div>
                <div className="blog-content">
                  <p>
                    {description}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Fragment>
  )
}

export default VideoSingle