import './index.scss'

interface IProps {
  embedId: string;
  title?: string
}

const YoutubeEmbed = ({ embedId, title }: IProps) => {
  return <div className="youtube-iframe">
    <iframe
      src={`https://www.youtube.com/embed/${embedId}`}
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      allowFullScreen
      {...{ title }}
    />
  </div>
}

export default YoutubeEmbed