import { StrictMode } from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import { Provider } from 'react-redux'

import store from './store'
import App from "./App.js";


ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
	<StrictMode>
		<BrowserRouter>
			<Provider {...{ store }} >
				<App />
			</Provider>
		</BrowserRouter>
	</StrictMode>
);
