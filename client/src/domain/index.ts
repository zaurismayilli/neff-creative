import aboutApi from "./about/aboutApi";
import authApi from "./auth/authApi";
import { api as blogApi } from "./blog";
import categoryApi from "./category/categoryApi";
import contactApi from "./contact/contactApi";
import galleryApi from "./gallery/galleryApi";
import serviceApi from "./service/serviceApi";
import videoApi from "./video/videoApi";
import subscribeApi from "./subscribe/subscribeApi";

export const reducer = {
	[authApi.reducerPath]: authApi.reducer,
	[aboutApi.reducerPath]: aboutApi.reducer,
	[blogApi.reducerPath]: blogApi.reducer,
	[categoryApi.reducerPath]: categoryApi.reducer,
	[galleryApi.reducerPath]: galleryApi.reducer,
	[videoApi.reducerPath]: videoApi.reducer,
	[serviceApi.reducerPath]: serviceApi.reducer,
	[contactApi.reducerPath]: contactApi.reducer,
	[contactApi.reducerPath]: contactApi.reducer,
	[subscribeApi.reducerPath]: subscribeApi.reducer,
};

export const middleware = [
	authApi.middleware,
	aboutApi.middleware,
	blogApi.middleware,
	categoryApi.middleware,
	galleryApi.middleware,
	videoApi.middleware,
	serviceApi.middleware,
	contactApi.middleware,
	subscribeApi.middleware,
];
