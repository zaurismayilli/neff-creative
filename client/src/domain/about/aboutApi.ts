import axiosBaseQuery from "@/api";
import { createApi } from "@reduxjs/toolkit/query/react";

const aboutApi = createApi({
	baseQuery: axiosBaseQuery(),
	tagTypes: ["about"],
	reducerPath: "about",
	endpoints: (build) => ({
		fetchAbout: build.query({
			query: () => ({
				url: `/about`,
				method: "GET",
			}),
			providesTags: ["about"],
		}),
	}),
});

export default aboutApi;

export const aboutHooks = {
	useAbout: aboutApi.useFetchAboutQuery,
};
