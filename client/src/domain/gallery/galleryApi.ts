import axiosBaseQuery from "@/api";
import { createApi } from "@reduxjs/toolkit/query/react";

const galleryApi = createApi({
	baseQuery: axiosBaseQuery(),
	tagTypes: ["gallery"],
	reducerPath: "gallery",
	endpoints: (build) => ({
		fetchGalleryByCategoryId: build.query({
			query: ({ categoryId }) => ({
				url: `/gallery?categoryId=${categoryId}`,
				method: "GET",
			}),
			providesTags: ["gallery"],
		}),
	}),
});

export default galleryApi;

export const galleryHooks = {
	useGallery: galleryApi.useFetchGalleryByCategoryIdQuery,
};
