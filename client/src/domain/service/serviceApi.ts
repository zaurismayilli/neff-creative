import axiosBaseQuery from "@/api";
import { createApi } from "@reduxjs/toolkit/query/react";

const serviceApi = createApi({
	baseQuery: axiosBaseQuery(),
	tagTypes: ["service"],
	reducerPath: "service",
	endpoints: (build) => ({
		fetchService: build.query({
			query: () => ({
				url: `/service`,
				method: "GET",
			}),
			providesTags: ["service"],
		}),
	}),
});

export default serviceApi;

export const serviceHooks = {
	useService: serviceApi.useFetchServiceQuery,
};
