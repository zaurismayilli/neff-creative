import axiosBaseQuery from "@/api";
import storage from "@/shared/utils/storage";
import { createApi } from "@reduxjs/toolkit/query/react";

const authApi = createApi({
	baseQuery: axiosBaseQuery(),
	tagTypes: ["auth"],
	reducerPath: "auth",
	endpoints: (build) => ({
		signIn: build.mutation({
			query: (data) => ({
				url: "/auth/signin",
				method: "POST",
				data,
			}),
		}),
		signUp: build.mutation({
			query: (data) => ({
				url: "/auth/signup",
				method: "POST",
				data,
			}),
		}),
		signOut: build.mutation({
			query: () => ({
				url: "/auth/signout",
				method: "POST",
			}),
		}),
		refreshToken: build.query({
			query: () => ({
				url: "/auth/token/refresh",
				method: "GET",
			}),
		}),
		getMe: build.query({
			query: () => ({
				url: "/auth/getme",
				method: "GET",
				headers: {
					Authorization: `Bearer ${storage.getAuthToken().accessToken}`,
				},
			}),
			providesTags: ["auth"],
		}),
		updateProfile: build.mutation({
			query: (data) => ({
				url: "/auth/update/profile",
				method: "PATCH",
				data,
			}),
			invalidatesTags: ["auth"],
		}),
		updatePassword: build.mutation({
			query: (data) => ({
				url: "/auth/update/password",
				method: "PATCH",
				data,
			}),
		}),
	}),
});

export default authApi;

export const authHooks = {
	useSignIn: authApi.useSignInMutation,
	useSignUp: authApi.useSignUpMutation,
	useSignOut: authApi.useSignOutMutation,
	useRefreshToken: authApi.useRefreshTokenQuery,
	useGetMe: authApi.useGetMeQuery,
	useUpdateProfile: authApi.useUpdateProfileMutation,
	useUpdatePassword: authApi.useUpdatePasswordMutation,
};
