import axiosBaseQuery from "@/api";
import { createApi } from "@reduxjs/toolkit/query/react";

const subscribeApi = createApi({
	baseQuery: axiosBaseQuery(),
	reducerPath: "subscribe",
	endpoints: (build) => ({
		addSubscribe: build.mutation({
			query: (body) => ({
				url: "/subscribe",
				method: "POST",
				data: body,
			}),
		}),
	}),
});

export default subscribeApi;

export const subscribeHooks = {
	addSubscribe: subscribeApi.useAddSubscribeMutation,
};
