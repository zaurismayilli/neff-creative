import axiosBaseQuery from "@/api";
import { createApi } from "@reduxjs/toolkit/query/react";

const blogApi = createApi({
	baseQuery: axiosBaseQuery(),
	tagTypes: ["blog"],
	reducerPath: "blog",
	endpoints: (build) => ({
		fetchBlog: build.query({
			query: ({ page = 1, limit = 3 }) => ({
				url: `/blog?page=${page}&limit=${limit}`,
				method: "GET",
			}),
			providesTags: ["blog"],
		}),
		fetchBlogById: build.query({
			query: ({ id }) => ({
				url: `/blog/${id}`,
				method: "GET",
			}),
			providesTags: ["blog"],
		}),
		addBlog: build.mutation({
			query: (body) => ({
				url: "/blog",
				method: "POST",
				data: body,
			}),
			invalidatesTags: ["blog"],
		}),
	}),
});

export default blogApi;

export const blogHooks = {
	useBlog: blogApi.useFetchBlogQuery,
	useBlogById: blogApi.useFetchBlogByIdQuery,
	addBlog: blogApi.useAddBlogMutation,
};
