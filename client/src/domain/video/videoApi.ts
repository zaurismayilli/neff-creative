import axiosBaseQuery from "@/api";
import { createApi } from "@reduxjs/toolkit/query/react";

const videoApi = createApi({
	baseQuery: axiosBaseQuery(),
	tagTypes: ["video"],
	reducerPath: "video",
	endpoints: (build) => ({
		fetchVideo: build.query({
			query: ({ page = 1, limit = 3 }) => ({
				url: `/video?page=${page}&limit=${limit}`,
				method: "GET",
			}),
			providesTags: ["video"],
		}),
		fetchVideoById: build.query({
			query: ({ id }) => ({
				url: `/video/${id}`,
				method: "GET",
			}),
			providesTags: ["video"],
		}),
	}),
});

export default videoApi;

export const videoHooks = {
	useVideo: videoApi.useFetchVideoQuery,
	useVideoById: videoApi.useFetchVideoByIdQuery,
};
