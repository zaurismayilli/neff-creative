import axiosBaseQuery from "@/api";
import { createApi } from "@reduxjs/toolkit/query/react";

const contactApi = createApi({
	baseQuery: axiosBaseQuery(),
	tagTypes: ["contact"],
	reducerPath: "contact",
	endpoints: (build) => ({
		fetchContact: build.query({
			query: () => ({
				url: `/contact`,
				method: "GET",
			}),
			providesTags: ["contact"],
		}),
	}),
});

export default contactApi;

export const contactHooks = {
	useContact: contactApi.useFetchContactQuery,
};
