import axiosBaseQuery from "@/api";
import { createApi } from "@reduxjs/toolkit/query/react";

const categoryApi = createApi({
	baseQuery: axiosBaseQuery(),
	tagTypes: ["category"],
	reducerPath: "category",
	endpoints: (build) => ({
		fetchCategory: build.query({
			query: () => ({
				url: "/category",
				method: "GET",
			}),
		}),
	}),
});

export default categoryApi;

export const categoryHooks = {
	useCategory: categoryApi.useFetchCategoryQuery,
};
