import React from 'react';

class Blog extends React.Component {
    render () {
        return (
            <div id="blog-form-block">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th className="wh-nowrap"> Order № </th>
                            <th> Blog name </th>
                            <th className="wh-nowrap"> Author </th>
                            <th className="wh-nowrap"> Controls </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td > 1 </td>
                            <td> Resident evil remake 2 rumour </td>
                            <td className="wh-nowrap">Scott Kennedy </td>
                            <td className="wh-nowrap">
                                <button type="button" className="mini-button btn-primary waves-effect"> <i className="fa fa-edit"></i> </button>
                                <button type="button" className="mini-button btn-danger waves-effect"> <i className="fa fa-remove"></i> </button>
                                <button type="button" className="mini-button btn-warning waves-effect"> <i className="fa fa-eye"></i> </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Blog