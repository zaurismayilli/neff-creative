import React from 'react';

class Slider extends React.Component {
    render () {
        return (
            <div id="slider-form-block">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th className="wh-nowrap"> Order № </th>
                            <th> Slider name </th>
                            <th> Slider link </th>
                            <th className="wh-nowrap"> Slider image </th>
                            <th className="wh-nowrap"> Controls </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td > 1 </td>
                            <td> Resident evil remake 2 rumour </td>
                            <td> www.youtube.com </td>
                            <td className="wh-nowrap">  <div className="mini-image"> <img src="assets/image/slider/image1.jpg" alt="" />  </div> </td>
                            <td className="wh-nowrap">
                                <button type="button" className="mini-button btn-primary waves-effect"> <i className="fa fa-edit"></i> </button>
                                <button type="button" className="mini-button btn-danger waves-effect"> <i className="fa fa-remove"></i> </button>
                                <button type="button" className="mini-button btn-warning waves-effect"> <i className="fa fa-eye"></i> </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Slider