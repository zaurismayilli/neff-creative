import React from 'react';

class Gallery extends React.Component {
    render () {
        return (
            <div id="gallery-form-block">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th className="wh-nowrap"> Order № </th>
                            <th> Gallery title </th>
                            <th> Gallery category </th>
                            <th className="wh-nowrap"> Gallery image </th>
                            <th className="wh-nowrap"> Controls </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td > 1 </td>
                            <td> Resident Evil remake 2 </td>
                            <td> Game </td>
                            <td className="wh-nowrap">  <div className="mini-image"> <img src="assets/image/slider/image1.jpg" alt="" />  </div> </td>
                            <td className="wh-nowrap">
                                <button type="button" className="mini-button btn-primary waves-effect"> <i className="fa fa-edit"></i> </button>
                                <button type="button" className="mini-button btn-danger waves-effect"> <i className="fa fa-remove"></i> </button>
                                <button type="button" className="mini-button btn-warning waves-effect"> <i className="fa fa-eye"></i> </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Gallery;