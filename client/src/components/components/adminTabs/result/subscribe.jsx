import React from 'react';
import Loading from '../../loading/loading'
class Subscribe extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            subscribe: [],
            loading: false
        }
    }

    componentDidMount () {
        this.showSubs();
    }

    showSubs  = () => {
        this.setState({
            loading: true
        })
        const bodyRequest = {
            query: `
                query {
                    subscribe{
                        name
                        email
                        subject
                        text
                    }
                }
            `
        }
        fetch("http://localhost:3000/graphql", {
            method: "POST",
            body: JSON.stringify(bodyRequest),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => {
            if(res.status !== 200 && res.status !== 201) {
                throw new Error('failed error response')
            }

            return res.json();
        })
        .then(resData=> {
            this.setState({
                loading: false,
                subscribe:resData.data.subscribe
            })
        })
        .catch(err => {
            console.log(err);
            this.setState({
                loading: false
            })
        })
    }
    render () {
      
        return (
            <React.Fragment>

            <table className="table table-striped">
                <thead>
                    <tr>
                        <th className="wh-nowrap"> Order № </th>
                        <th> Slider name </th>
                        <th> Slider link </th>
                        <th> Slider link </th>
                        <th className="wh-nowrap"> Slider image </th>
                        <th className="wh-nowrap"> Controls </th>
                    </tr>
                </thead>
            </table>
            <table className="table table-striped">
                {this.state.loading ? <Loading/> :
                <tbody>
                    {
                        this.state.subscribe.map((subs, i) => {
                            return (
                                <React.Fragment  key={i}>
                                    <tr key={i}>
                                        <td> {i + 1} </td>
                                        <td> {subs.name} </td>
                                        <td> {subs.email} </td>
                                        <td className="wh-nowrap">  {subs.subject} </td>
                                        <td className="wh-nowrap">  {subs.text} </td>
                                        <td className="wh-nowrap">
                                            <button type="button" className="mini-button btn-primary waves-effect"> <i className="fa fa-edit"></i> </button>
                                            <button type="button" className="mini-button btn-danger waves-effect"> <i className="fa fa-remove"></i> </button>
                                            <button type="button" className="mini-button btn-warning waves-effect"> <i className="fa fa-eye"></i> </button>
                                        </td>
                                    </tr>
                                </React.Fragment>
                            );
                        })
                    }
                </tbody>
                }
            </table>
            </React.Fragment>
        )
    }
}

export default Subscribe