import React from 'react';

class Slider extends React.Component {
    render () {
        return (
            <div id="slider-form-block">
                <form>
                    <div className="col-md-6">
                        <div className="default-element">
                            <input type="text" className="form-control" placeholder="Username" />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="default-element">
                            <input type="text" className="form-control" placeholder="Username" />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="default-element">
                            <input type="text" className="form-control" placeholder="Username" />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="default-element">
                            <input type="text" className="form-control" placeholder="Username" />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="default-element">
                            <input type="text" className="form-control" placeholder="Username" />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="default-element">
                            <input type="text" className="form-control" placeholder="Username" />
                        </div>
                    </div>
                    <div className="col-md-12 text-right">
                        <button className="link-md blue waves-effect" type="submit">  Submit </button>
                    </div>
                    <div className="clearfix"></div>
                </form>
            </div>
        )
    }
}

export default Slider