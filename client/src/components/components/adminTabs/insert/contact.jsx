import React from 'react';
import Loading from '../../loading/loading'
class Contact extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            created: false,
            loading:false,
            mAddrs: '',
            email:'',
            mobile:'',
            bAddrs:''
        }
        this.mAddrs = React.createRef();
        this.email  = React.createRef();
        this.mobile = React.createRef();
        this.bAddrs = React.createRef();
    }

    componentDidMount () {
        this.showContact();
    }

    showContact = () => {
        const bodyRequest = {
            query: `
                query {
                    contact{
                        _id
                        email
                        mobile
                        mAddrs
                        bAddrs
                    }
                }
            `
        }

        fetch("http://localhost:3000/graphql", {
            method: "POST",
            body:JSON.stringify(bodyRequest),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => {
            if(res.status !== 200 && res.status !== 201) {
                throw new Error('failed error')
            }
            return res.json();
        })
        .then(resData => {
           this.setState({
                loading:false,
                mAddrs:resData.data.contact[0].mAddrs,
                email:resData.data.contact[0].email,
                mobile:resData.data.contact[0].mobile,
                bAddrs:resData.data.contact[0].bAddrs
           });
        })
    }

    sendForm = (val) => {
        val.preventDefault();
        const mAddrs    = this.mAddrs.current.value;
        const email     = this.email.current.value;
        const mobile    = this.mobile.current.value;
        const bAddrs    = this.bAddrs.current.value;

        if(mAddrs.trim().length === 0 || 
        email.trim().length === 0 ||
        mobile.trim().length === 0 ||
        bAddrs.trim().length === 0  ) {
            return ;
        }
        this.setState({
            loading:true
        })
        const bodyRequest = {
            query: `
                mutation($mAddrs: String!, $email: String!, $mobile: Float!, $bAddrs: String!) {
                    addContact(inputContact:{mAddrs: $mAddrs, email: $email, mobile:$mobile, bAddrs: $bAddrs}){
                        mAddrs
                        email
                        mobile
                        bAddrs
                    }
                }
            `,
            variables: {
                mAddrs,
                email,
                mobile: parseInt(mobile),
                bAddrs
            }
        }

        fetch("http://localhost:3000/graphql", {
            method: "POST",
            body: JSON.stringify(bodyRequest),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => {
            if(res.status !== 200 && res.status !== 201) {
                throw new Error("error failed")
            }
            return res.json();
        })
        .then(resData => {
            this.setState({
                loading:false,
                created:true
            })
        })
        .catch(err => {
            this.setState({
                loading:false
            })
            console.log(err)
        })
    }

    onmAddrs = (val) => {
        this.setState({
            mAddrs : val.target.value
        })
    }

    onEmail = (val) => {
        this.setState({
            email : val.target.value
        })
    }

    onMobile = (val) => {
        this.setState({
            mobile : val.target.value
        })
    }

    onbAddrs = (val) => {
        this.setState({
            bAddrs : val.target.value
        })
    }

    render () {
        return (
            <div id="slider-form-block">
                {!this.state.loading && this.state.created === true && <span className="alert alert-success" style={{marginBottom: '20px', display:'block'}}> updated succesfully </span> }
                {this.state.loading && <Loading /> }
                <form onSubmit={this.sendForm.bind(this)} >
                    <div className="col-md-6">
                        <div className="default-element">
                            <input value={this.state.mAddrs} onChange={this.onmAddrs.bind(this)} ref={this.mAddrs} type="text" className="form-control" placeholder="mini Address" />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="default-element">
                            <input value={this.state.email} onChange={this.onEmail.bind(this)} ref={this.email} type="email" className="form-control" placeholder="Email" />
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="default-element">
                            <input value={this.state.mobile} onChange={this.onMobile.bind(this)} ref={this.mobile} type="number" className="form-control" placeholder="mobile" />
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="default-element">
                            <textarea value={this.state.bAddrs} onChange={this.onbAddrs.bind(this)} ref={this.bAddrs} type="text" className="form-control" placeholder="big Address"></textarea>
                        </div>
                    </div>
                    <div className="col-md-12 text-right">
                        <button className="link-md blue waves-effect" type="submit">  Submit </button>
                    </div>
                    <div className="clearfix"></div>
                </form>
            </div>
        )
    }
}

export default Contact