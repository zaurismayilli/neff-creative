import React from 'react';

import AuthContext from '../../../../context/auth-context'

import Loading from '../../loading/loading'

class Blog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            action: false,
            isLoading: false,
            blogLoading: false,
            category: []
        }
        this.title      = React.createRef();
        this.content    = React.createRef();
        this.image      = React.createRef();
        this.date       = new Date().toLocaleDateString('en-US').toString();
        this.category   = React.createRef();
    }

    static contextType = AuthContext;

    componentDidMount () {
        this.showCategory();
    }

    showCategory = () => {
        const bodyRequest = {
            query : `
                query {
                    categories{
                        catName
                        _id
                    }
                }
            `
        }
        fetch("http://localhost:3000/graphql", {
            method: "POST",
            body: JSON.stringify(bodyRequest),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => {
            if(res.status !== 200 && res.status !== 201) {
                throw new Error ('failed response')
            }

            return res.json();
        })
        .then(resData => {
            this.setState({
                category:resData.data.categories,
                isLoading: true
            })
        })
        .catch(err => {
            console.log(err)
        })
    }

    sendForm = (value) =>{
        this.setState({
            blogLoading: true
        })
        value.preventDefault();
        const title = this.title.current.value;
        const content = this.content.current.value;
        const image = this.image.current.value;
        const date = this.date;
        const category = this.category.current.value;
        const creator = this.context.userId;
        if(title.trim().length === 0 ||
        content.trim().length === 0 ||
        image.trim().length === 0 ||
        date.trim().length === 0 ||
        category.trim().length === 0) {
            return ;
        }   

        const bodyRequest = {
            query: `
                mutation ($title: String!, $content: String!, $image: String!, $date: String!,  $category: ID!, $creator: ID!) {
                    addBlog(inputBlog:{title: $title, content: $content, image: $image, date: $date, category: $category, creator: $creator  }){
                        _id
                        title
                        content
                        image
                        category{
                            catName
                        }
                        creator{
                            email
                        }
                    }
                }
            `,
            variables: {
                title,
                content,
                image,
                date,
                category,
                creator
            }
        }
        fetch("http://localhost:3000/graphql", {
            method: "POST",
            body: JSON.stringify(bodyRequest),
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.context.token
            }
        })
        .then(res => {
            if(res.status !== 200 && res.status !== 201) {
                throw new Error ('error response failed')
            }
            return res.json();
        })
        .then(resData => {
            this.setState({
                action:true,
                blogLoading:false
            })
        })
        .catch(err => {
            console.log(err);
            this.setState({
                blogLoading:false
            })
        })
    }
    render () {
        const cats = this.state.category.map(cats => {
            return <option key={cats._id} value={cats._id} > {cats.catName} </option>
        })
        return (
            <div id="blog-form-block">
                {this.state.blogLoading && <Loading /> }
                {this.state.action ?  <span className="alert alert-success" > blog succesfulley added ... .. . </span> :
                    <form onSubmit={this.sendForm.bind(this)} >
                        <div className="col-md-6">
                            <div className="default-element">
                                <input ref={this.title} type="text" className="form-control" placeholder="Blog Title" />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="default-element">
                                <input ref={this.image} type="text" className="form-control" placeholder="Blog image" />
                            </div>
                        </div>
                        <div className="col-md-12">
                            <div className="default-element">
                                <textarea ref={this.content} className="form-control" placeholder="Blog Content"></textarea>
                            </div>
                        </div>
                        <div className="col-md-12">
                            <div className="default-element">
                                <select ref={this.category} className="form-control">
                                    <option value=""> choosen category </option>
                                    {this.state.isLoading && cats }
                                </select>
                            </div>
                        </div>
                        <div className="col-md-12 text-right">
                            <button className="link-md blue waves-effect" type="submit">  Submit </button>
                        </div>
                        <div className="clearfix"></div>
                    </form>
                }
            </div>
        )
    }
}

export default Blog