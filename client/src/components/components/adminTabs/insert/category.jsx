import React from 'react';

class Category extends React.Component {
    constructor(props) {
        super(props);
        this.catName = React.createRef();
       this.state = {
            action: false
       }
    }


    sendForm = (value) => {
        value.preventDefault();
        const catName = this.catName.current.value;

        if(catName.trim().length === 0) {
            return ;
        }
        const bodyRequest = {
            query: `
                mutation($catName: String!) {
                    addCategory(inputCategory:{catName: $catName}){
                        catName
                        _id
                    }
                }
            `,
            variables: {
                catName
            }
        };
        fetch("http://localhost:3000/graphql", {
            method: "POST",
            body: JSON.stringify(bodyRequest),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => {
            if(res.status !== 200 && res.status !== 201) {
                throw new Error("failed response")
            }
            return res.json();
        })
        .then(resData => {
            this.setState({
                action: true
            })
        })
        .catch(err => {
            console.log(err)
        })
    }

    render () {
        return (
            <div id="blog-form-block">
                {this.state.action ? <span className="alert alert-success" > category successfully added </span> :
                    <form onSubmit={this.sendForm.bind(this)} >
                        <div className="col-md-6">
                            <div className="default-element">
                                <input type="text" ref={this.catName} className="form-control" placeholder="Category" />
                            </div>
                        </div>
                        <div className="col-md-12 text-right">
                            <button className="link-md blue waves-effect" type="submit">  Submit </button>
                        </div>
                        <div className="clearfix"></div>
                    </form>
                }
            </div>
        )
    }
}

export default Category