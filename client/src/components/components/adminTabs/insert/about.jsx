import React from 'react';
import AuthContext from   '../../../../context/auth-context'
import Loading from '../../loading/loading'
class About extends React.Component {
    static contextType = AuthContext;
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            updated: false,
            title:'dunya',
            content:'ikinci'
        }
        this.title = React.createRef();
        this.content = React.createRef();
    }

    componentDidMount () {
        this.showAbout();
    }

    showAbout = () => {
        this.setState({
            loading: true
        })
        const bodyRequest = {
            query: `
                query {
                    about {
                        title
                        content
                    }
                }
            `
        }

        fetch("http://localhost:3000/graphql", {
            method: "POST",
            body: JSON.stringify(bodyRequest),
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.context.token
            }
        })
        .then(res => {
            if(res.status !== 200 && res.status !== 201) {
                throw new Error ('failed response')
            }
            return res.json();
        })
        .then(resData => {
            this.setState({
                loading: false,
                title: resData.data.about[0].title,
                content: resData.data.about[0].content
            })
        })
        .catch(err => {
            console.log(err);
            this.setState({
                loading: false
            })
        })
    }

    sendForm = (value) => {
        this.setState({
            loading: true
        })
        value.preventDefault();
        const title     = this.title.current.value;
        const content   = this.content.current.value;

        if(title.trim().length === 0 || content.trim().length === 0) {
            return ;
        }

        let bodyRequest;

        bodyRequest = {
            query: `
                mutation($title: String!, $content: String!) {
                    addAbout(inputAbout:{title: $title, content: $content }) {
                        title
                        content
                    }
                }
            `,
            variables: {
                title,
                content
            }
        }

        const token = this.context.token
       fetch("http://localhost:3000/graphql", {
           method: "POST",
           body: JSON.stringify(bodyRequest),
           headers: {
               'Content-Type': 'application/json',
               Authorization: 'Bearer ' + token
           }
       })
       .then(res => {
           if(res.status !== 200 && res.status !== 201) {
               throw new Error('failed response')
           }
           return res.json();
       })
       .then(resData => {
            this.setState({
                loading: false,
                updated: true
            });
       })
       .catch(err => {
           console.log(err);
           this.setState({
            loading: false
        })
       })
    }

    myinputTitle = value => {
        this.setState({
            title:value.target.value
        })
    }

    myinputContent = content => {
        this.setState({
            content:content.target.value
        });
    }


    render () {
        return (
            <div id="blog-form-block">
                {this.state.updated && <span className="alert alert-success" style={{position: 'relative', top: '-23px'}} > About update succesfully </span> }
                 {this.state.loading ?  <Loading /> 
                :
                <form onSubmit={this.sendForm.bind(this)}>
                    <div className="col-md-12">
                        <div className="default-element">
                            <input value={this.state.title} onChange={this.myinputTitle.bind(this)} ref={this.title} type="text" className="form-control" placeholder="Title" />
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="default-element">
                            <textarea value={this.state.content} onChange={this.myinputContent.bind(this)} ref={this.content} className="form-control" placeholder="Content"></textarea>
                        </div>
                    </div>
                    <div className="col-md-12 text-right">
                        <button className="link-md blue waves-effect" type="submit">  Submit </button>
                    </div>
                    <div className="clearfix"></div>
                </form>
                }
                
            </div>
        )
    }
}

export default About