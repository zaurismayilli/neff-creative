import React from 'react';
import AuthContext from '../../../../context/auth-context'
import Loading from '../../loading/loading'
class Services extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            action: false
        }
        this.title = React.createRef();
        this.image = React.createRef();
    }

    static contextType = AuthContext;

    sendForm = (value) => {
        this.setState({
            loading: true
        })
        value.preventDefault();
        const title = this.title.current.value;
        const image = this.image.current.value;

        if(title.trim().length === 0 || image.trim().length === 0) {
            return ;
        }
        const bodyRequest = {
            query : `
                mutation($title: String!, $image: String!) {
                    addService(inputService:{title: $title, image: $image }) {
                        title
                        image
                    }
                }
            `,
            variables: {
                title,
                image
            }
        }
        fetch("http://localhost:3000/graphql", {
            method: "POST",
            body: JSON.stringify(bodyRequest),
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.context.token
            }
        })
        .then(res => {
            if(res.status !== 200 && res.status !== 201) {
                throw new Error("failed response")
            }

            return res.json();
        })
        .then(resData => {
            this.setState({
                loading: false,
                action: true
            })
        })
        .catch(err => {
            console.log(err);
            this.setState({
                loading: false
            })
        })
    }

    render () {
        return (
            <div id="blog-form-block">
                {this.state.loading && <Loading />}
                {!this.state.loading && this.state.action &&  <span className="alert alert-success" > succesfully added </span> }
                 <form onSubmit={this.sendForm.bind(this)} >
                    <div className="col-md-6">
                        <div className="default-element">
                            <input ref={this.title} type="text" className="form-control" placeholder="Title" />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="default-element">
                            <input ref={this.image} type="text" className="form-control" placeholder="Image" />
                        </div>
                    </div>
                    <div className="col-md-12 text-right">
                        <button className="link-md blue waves-effect" type="submit">  Submit </button>
                    </div>
                    <div className="clearfix"></div>
                </form>
            </div>
        )
    }
}

export default Services