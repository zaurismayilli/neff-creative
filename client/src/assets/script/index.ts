/*[---------- SEARCH FORM  ----------]*/
export default () => {
	const searchButton = document.getElementById("search-button");
	const closeSearch = document.getElementById("close-search");
	const mobileBtn = document.getElementById("mobile-btn");
	const bannerRemove = document.getElementById("banner-remove");

	document.getElementById("search-button")?.addEventListener("click", function () {
		document.getElementById("search-form")?.classList.add("active");
	});
	document.getElementById("close-search")?.addEventListener("click", function () {
		document.getElementById("search-form")?.classList.remove("active");
	});

	document.getElementById("mobile-btn")?.addEventListener("click", (e: any) => {
		e.target.closest("button").classList.toggle("active");
		document.querySelector(".menu")?.classList.toggle("active");
		document.body.classList.toggle("overflow");
		//e.target.closest("header").classList.toggle("full-height");
	});

	/*[---------- BANNER REMOVE  ----------]*/
	document.getElementById("banner-remove")?.addEventListener("click", () => {
		document.getElementById("banner-section")?.remove();
		document.getElementById("header")?.classList.add("top");
	});

	/*[---------- Waves effect buttons  ----------]*/
	document.querySelectorAll("a, button").forEach((item) => {
		item.classList.add("waves-effect");
	});

	/*[---------- Header fixed   ----------]*/
	let mouseDown = 0;
	document.addEventListener("scroll", () => {
		let topWindow = document.getElementById("header")?.offsetHeight || 0;
		let scrolled = document.documentElement.scrollTop;
		if (scrolled >= topWindow) {
			document.getElementById("header")?.classList.add("fixed-anime");
			if (document.body.getBoundingClientRect().top > mouseDown) {
				document.getElementById("header")?.classList.add("move-up");
			} else {
				document.getElementById("header")?.classList.remove("move-up");
			}
		} else {
			document.getElementById("header")?.classList.remove("fixed-anime");
		}
		mouseDown = document.body.getBoundingClientRect().top;
		let bodyHeight = document.documentElement.scrollHeight - document.documentElement.clientHeight;
		let indicator = (100 * document.documentElement.scrollTop) / bodyHeight;
		let topLiine: any = document.getElementById("top-line");
		topLiine["style"]["width"] = indicator + "%";
	});

	/*[---------- Auto dedect   ----------]*/
	function autoDedect(millisecond: any) {
		var times: number;
		window.onload = resetTimer;
		window.onmousemove = resetTimer;
		window.onmousedown = resetTimer;
		window.ontouchstart = resetTimer;
		window.onclick = resetTimer;
		window.onkeypress = resetTimer;
		window.addEventListener("scroll", resetTimer, true);

		function sleeping() {
			document.body.style.opacity = "0.8";
		}

		function resetTimer() {
			clearTimeout(times);
			times = setTimeout(sleeping, millisecond);
			document.body.style.opacity = "1";
		}
	}
	autoDedect(200000);
};
