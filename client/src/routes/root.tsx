import { ComponentPropsWithRef, LazyExoticComponent, Suspense } from 'react';
import { Route, Routes } from 'react-router-dom';
import Loading from '@/shared/Components/Loading';

import { routes } from './router';
import ProtectedRoute from './protectedRoute';

interface IRoot {
  PATH: string;
  ELEMENT: LazyExoticComponent<ComponentPropsWithRef<any>>;
  isProtected?: boolean
}

const Root = () => {
  const route = Object.entries(routes).map((item, key) => {
    const { ELEMENT, PATH, isProtected }: IRoot = item[1];
    return (
      <Route
        key={key}
        path={PATH}
        element={
          <Suspense fallback={<Loading />} >
            {isProtected ?
              <ProtectedRoute> <ELEMENT /> </ProtectedRoute>
              :
              <ELEMENT />
            }
          </Suspense>}
      />
    )
  })

  return <Routes> {route}</Routes>
}

export default Root;
