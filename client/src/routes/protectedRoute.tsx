import { useAuth } from "@/shared/hooks"
import { ReactNode } from "react";

const ProtectedRoute = ({ children }: { children: ReactNode }) => {
  const [isLoggedIn] = useAuth();
  return <>
    {isLoggedIn ? children : null}
  </>
}

export default ProtectedRoute