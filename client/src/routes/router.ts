import { lazy } from "react";

export const routes = {
	GLOBAL: {
		PATH: "/",
		ELEMENT: lazy(() => import("@/pages/home")),
	},
	HOME: {
		PATH: "/home",
		ELEMENT: lazy(() => import("@/pages/home")),
	},
	ABOUT: {
		PATH: "/about",
		ELEMENT: lazy(() => import("@/pages/about")),
	},
	BLOG: {
		PATH: "/blog",
		ELEMENT: lazy(() => import("@/pages/blog")),
	},
	BLOGSINGLE: {
		PATH: "/blog/:id",
		ELEMENT: lazy(() => import("@/pages/blogSingle")),
	},
	CONTACT: {
		PATH: "/contact",
		ELEMENT: lazy(() => import("@/pages/contact")),
	},
	GALLERY: {
		PATH: "/gallery",
		ELEMENT: lazy(() => import("@/pages/gallery")),
	},
	SERVICE: {
		PATH: "/service",
		ELEMENT: lazy(() => import("@/pages/service")),
	},
	VIDEO: {
		PATH: "/video",
		ELEMENT: lazy(() => import("@/pages/video")),
	},
	VIDEOSINGLE: {
		PATH: "/video/:id",
		ELEMENT: lazy(() => import("@/pages/videoSingle")),
	},
	LOGIN: {
		PATH: "/auth/signin",
		ELEMENT: lazy(() => import("@/pages/login")),
	},
	REGISTER: {
		PATH: "/auth/signup",
		ELEMENT: lazy(() => import("@/pages/register")),
	},
	PROFILE: {
		PATH: "/auth/profile",
		ELEMENT: lazy(() => import("@/pages/profile")),
		isProtected: true,
	},
	NOTFOUND: {
		PATH: "*",
		ELEMENT: lazy(() => import("@/shared/Components/NotFound")),
	},
};
