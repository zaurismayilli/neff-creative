import Layout from "./shared/layout";
import Routes from "./routes";

const App = () => (
	<Layout>
		<Routes />
	</Layout>
);

export default App;