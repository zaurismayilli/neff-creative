import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import tsconfigPaths from 'vite-tsconfig-paths';
import path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
      '@': path.resolve('./src'),
      '@assets': path.resolve('./src/assets'),
      '@shared': path.resolve('./src/shared'),
      '@pages': path.resolve('./src/pages'),
    },
  },
  server: {
		host: true,
		port: 1000,
		strictPort: true,
	},
	plugins: [react(), tsconfigPaths()],
});
