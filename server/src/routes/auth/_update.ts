import { Response } from "express";
import { ObjectId } from "mongodb";
import bcrypt from "bcryptjs";
import _ from "lodash";

import { NewError, NewSuccess } from "../../utils/error";
import { IRequest } from "../../models/request";
import { generateMultiToken, userIdIdParsedByToken } from "../../utils/token";
import { ERROR } from "../../constants/error";
import uploader from "../../utils/uploader";

export const tokenController = async (req: IRequest, res: Response) => {
	try {
		const { refreshToken } = req?.body;

		const user = await req.store.auth.getUserByData({ refreshToken });
		console.log(user)

		if (!user) return NewError({ data: ERROR.AUTHENTICATION.TOKEN.EXPIRE_REFRESH_TOKEN, res });

		const { firstname, email, userId } = user;

		const payload = {
			firstname,
			email,
			userId,
		};
		const o_userId = new ObjectId(userId);

		const multiToken = await generateMultiToken({ payload });
		await req.store.auth.update({ userId: o_userId, data: multiToken });
		return NewSuccess({ data: multiToken, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};

export const updateProfileController = async (req: IRequest, res: Response) => {
	try {
		const { body } = req;
		const { firstname, lastname } = body;
		const file = req?.files && req.files[0];

		if (_.isEmpty(firstname) && _.isEmpty(lastname) && _.isEmpty(file)) {
			return NewError({ data: ERROR.COMMON_ERROR.MISSING_FIELDS(body), res });
		}

		const token: any = req?.headers.authorization?.split("Bearer ")[1];

		const userId = await userIdIdParsedByToken({ token });

		if (!userId) return NewError({ data: "there is no user id", res });

		const { image } = await req.store.auth.getUserByData({ userId });

		let newImage;

		const data = { ...body };

		if (file) {
			newImage = uploader.update(image, file);
			data.image = newImage;
		}

		await req.store.auth.update({
			userId,
			data,
		});

		const response = {
			data,
			updated: true,
		};

		return NewSuccess({ data: response, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};

export const updatePasswordController = async (req: IRequest, res: Response) => {
	try {
		const token: any = req?.headers.authorization?.split("Bearer ")[1];
		const { password_old, password_new, password_new_repeat } = req?.body;

		if (password_new !== password_new_repeat)
			return NewError({ data: ERROR.AUTHENTICATION.VALIDATION.DO_NOT_MATCH_PASSWORD, res });

		const userId = await userIdIdParsedByToken({ token });

		const user = await req.store.auth.getUserByData({ userId });
		if (!user) return NewError({ data: ERROR.AUTHENTICATION.VALIDATION.DO_NOT_EXIST_USER, res });

		const passwordMatch = await bcrypt.compare(password_old, user.password);

		if (!passwordMatch)
			return NewError({ data: ERROR.AUTHENTICATION.VALIDATION.INCORRECT_PASSWORD, res });

		const newPassword = await bcrypt.hash(password_new, 10);

		await req.store.auth.update({ userId, data: { password: newPassword } });

		return NewSuccess({ data: { password: newPassword }, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
