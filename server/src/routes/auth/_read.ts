import { Response } from "express";

import { NewSuccess, NewError } from "../../utils/error";
import { IRequest } from "../../models/request";
import { userIdIdParsedByToken } from "../../utils/token";
import { ERROR } from "../../constants/error";

export const getMeController = async (req: IRequest, res: Response) => {
	try {
		const token: any = req?.headers.authorization?.split("Bearer ")[1];

		const userId = await userIdIdParsedByToken({ token });

		if (!userId) return NewError({ data: ERROR.AUTHENTICATION.VALIDATION.USER_NOT_FOUND, res });

		const { firstname, lastname, email, image } = await req.store.auth.getUserByData({ userId });

		return NewSuccess({ data: { firstname, lastname, email, image }, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
