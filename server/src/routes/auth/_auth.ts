import { Response } from "express";
import { ObjectId } from "mongodb";
import bcrypt from "bcryptjs";

import { NewError, NewSuccess } from "../../utils/error";
import { IRequest } from "../../models/request";
import { ERROR } from "../../constants/error";
import { generateMultiToken, userIdIdParsedByToken } from "../../utils/token";

export const signInController = async (req: IRequest, res: Response) => {
	try {
		const { email, password } = req?.body;
		const user = await req.store.auth.getUserByData({ email });
		if (!user)
			return NewError({ code: 404, data: ERROR.AUTHENTICATION.VALIDATION.USER_NOT_FOUND, res });

		const { firstname, password: userPassword, userId } = user;

		const isPasswordMatch = await bcrypt.compare(password, userPassword);

		if (!isPasswordMatch)
			return NewError({ data: ERROR.AUTHENTICATION.VALIDATION.INCORRECT_ACCOUNT, res });

		const payload = {
			firstname,
			email,
			userId,
		};

		const multiToken = await generateMultiToken({ payload });
		const o_userId = new ObjectId(userId);
		await req.store.auth.update({ userId: o_userId, data: multiToken });

		return NewSuccess({ data: multiToken, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};

export const signUpController = async (req: IRequest, res: Response) => {
	try {
		const { email, password, password_repeat, firstname, lastname } = req?.body;

		const isUserExist = await req.store.auth.getUserByData({ email });

		if (!!isUserExist)
			return NewError({ data: ERROR.AUTHENTICATION.VALIDATION.USER_ALREADY_EXIST, res });

		if (password !== password_repeat)
			return NewError({ data: ERROR.AUTHENTICATION.VALIDATION.DO_NOT_MATCH_PASSWORD, res });
		const userId = new ObjectId();
		const payload = {
			firstname,
			email,
			userId,
		};

		const multiToken = await generateMultiToken({ payload });

		const encryptedPassword = await bcrypt.hash(password, 10);

		const user = {
			...payload,
			lastname,
			password: encryptedPassword,
			created_at: Date.now(),
			image: {
				filename: "",
				directory: "",
				path: "",
			},
			...multiToken,
		};

		await req.store.auth.signUp(user);

		return NewSuccess({ data: multiToken, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};

export const signOutController = async (req: IRequest, res: Response) => {
	try {
		const token: any = req?.headers.authorization?.split("Bearer ")[1];

		const userId = await userIdIdParsedByToken({ token });

		await req.store.auth.signOut({ userId });
		return NewSuccess({ data: ERROR.AUTHENTICATION.VALIDATION.SIGN_OUT, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
