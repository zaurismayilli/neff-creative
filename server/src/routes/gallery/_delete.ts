import { Response } from "express";

import { IRequest } from "../../models/request";
import { NewError, NewSuccess } from "../../utils/error";
import { ObjectId } from "mongodb";
import uploader from "../../utils/uploader";

export const deleteController = async (req: IRequest, res: Response) => {
	try {
		const { id } = req.params;
		const o_id = new ObjectId(id);

		const gallery = await req.store.gallery.getById({ id: o_id });

		await req.store.gallery.delete({ id: o_id });
		await uploader.delete(gallery.image);

		const response = {
			id: o_id,
			deleted: true,
		};

		return NewSuccess({ data: response, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
