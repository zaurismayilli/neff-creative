import { Response } from "express";
import { ObjectId } from "mongodb";
import _ from "lodash";

import { NewSuccess, NewError } from "../../utils/error";
import { IRequest } from "../../models/request";
import { ERROR } from "../../constants/error";

export const getListController = async (req: IRequest, res: Response) => {
	try {
		const { limit, page } = req.query;

		const count = await req.store.blog.getList({ limit: 0, page: 1 });
		const data = await req.store.gallery.getList({ limit, page });

		const response = {
			gallery: data,
			count: count.length,
		};

		return NewSuccess({ data: response, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};

export const getByIdController = async (req: IRequest, res: Response) => {
	try {
		const { id } = req.params;
		const o_id = new ObjectId(id);
		const data = await req.store.gallery.getById({ id: o_id });

		return NewSuccess({ data, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
