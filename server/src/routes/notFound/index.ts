import { Response } from "express";

import { NewError } from "../../utils/error";
import { IRequest } from "../../models/request";
import { ERROR } from "../../constants/error";

export const notFoundController = async (req: IRequest, res: Response) => {
	return NewError({ code: 404, data: ERROR.COMMON_ERROR.NOT_FOUND, res });
};
