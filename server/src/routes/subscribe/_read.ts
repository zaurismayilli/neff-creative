import { Response } from "express";

import { NewSuccess, NewError } from "../../utils/error";
import { IRequest } from "../../models/request";

export const getListController = async (req: IRequest, res: Response) => {
	const { limit, page } = req.query;
	try {
		const count = await req.store.subscribe.getList({ limit: 0, page: 1 });
		const data = await req.store.subscribe.getList({ limit, page });
		const response = {
			subscribe: data,
			count: count.length,
		};

		return NewSuccess({ data: response, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
