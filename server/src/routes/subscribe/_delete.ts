import { Response } from "express";
import { ObjectId } from "mongodb";

import { IRequest } from "../../models/request";
import { NewError, NewSuccess } from "../../utils/error";

export const deleteController = async (req: IRequest, res: Response) => {
	try {
		const { id } = req.params;
		const o_id = new ObjectId(id);

		await req.store.subscribe.delete({ id: o_id });

		const response = {
			id: o_id,
			deleted: true,
		};

		return NewSuccess({ data: response, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
