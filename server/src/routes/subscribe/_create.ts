import { Response } from "express";

import { NewError, NewSuccess } from "../../utils/error";
import { IRequest } from "../../models/request";

export const createController = async (req: IRequest, res: Response) => {
	try {
		const { body } = req;

		const data = {
			...body,
			created_at: new Date().getTime(),
		};

		await req.store.subscribe.create(data);

		return NewSuccess({ data, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
