import { getController } from "./_read";
import { updateController } from "./_update";

export { updateController, getController };
