import { Response } from "express";

import { IRequest } from "../../models/request";
import { NewError, NewSuccess } from "../../utils/error";

export const updateController = async (req: IRequest, res: Response) => {
	try {
		const { body } = req;

		await req.store.about.update(body);

		const response = {
			data: body,
			updated: true,
		};
		return NewSuccess({ data: response, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
