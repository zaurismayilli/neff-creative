import { Router } from "express";

import {
	createController as blogCreateController,
	updateController as blogUpdateController,
	deleteController as blogDeleteController,
	getListController as blogGetListController,
	getByIdController as blogGetByIdController,
} from "./blog";

import {
	createController as categoryCreateController,
	deleteController as categoryDeleteController,
	getListController as categoryGetListController,
	updateController as categoryUpdateController,
	getByIdController as categoryGetByIdController,
} from "./category";

import {
	updateController as contactUpdateController,
	getController as contactGetController,
} from "./contact";

import {
	updateController as aboutUpdateController,
	getController as aboutGetController,
} from "./about";

import {
	createController as serviceCreateController,
	deleteController as serviceDeleteController,
	getListController as serviceGetListController,
	getByIdController as serviceGetByIdController,
	updateController as serviceUpdateController,
} from "./service";

import {
	createController as galleryCreateController,
	updateController as galleryUpdateController,
	deleteController as galleryDeleteController,
	getListController as galleryGetListController,
	getByIdController as galleryGetByIdController,
} from "./gallery";

import {
	createController as videoCreateController,
	updateController as videoUpdateController,
	deleteController as videoDeleteController,
	getListController as videoGetListController,
	getByIdController as videoGetByIdController,
} from "./video";

import {
	createController as subscribeCreateController,
	deleteController as subscribeDeleteController,
	getListController as subscribeGetListController,
} from "./subscribe";

import {
	signInController,
	signUpController,
	getMeController,
	updateProfileController,
	updatePasswordController,
	tokenController,
	signOutController,
} from "./auth";

import {
	blogInputs,
	categoryInputs,
	galleryInputs,
	passwordInputs,
	serviceInputs,
	signInInputs,
	signUpInputs,
	videoInputs,
	subscribeInputs,
} from "../constants/inputs";
import { createValidation } from "../middlewares/validation";
import { authMiddleware } from "../middlewares/auth";
import uploader from "../utils/uploader";

import { notFoundController } from "./notFound";

const router = Router();

router.post(
	"/blog",
	uploader.multer(blogInputs).any(),
	createValidation(blogInputs),
	blogCreateController
);
router.patch("/blog/:id", uploader.multer().any(), blogUpdateController);
router.get("/blog", blogGetListController);
router.get("/blog/:id", blogGetByIdController);
router.delete("/blog/:id", blogDeleteController);

router.post("/category", createValidation(categoryInputs), categoryCreateController);
router.get("/category", authMiddleware, categoryGetListController);
router.get("/category/:id", categoryGetByIdController);
router.put("/category/:id", authMiddleware, categoryUpdateController);
router.delete("/category/:id", categoryDeleteController);

router.get("/contact", contactGetController);
router.patch("/contact", contactUpdateController);

router.get("/about", aboutGetController);
router.patch("/about", aboutUpdateController);

router.post(
	"/service",
	uploader.multer(serviceInputs).any(),
	createValidation(serviceInputs),
	serviceCreateController
);
router.get("/service", serviceGetListController);
router.get("/service/:id", authMiddleware, serviceGetByIdController);
router.delete("/service/:id", authMiddleware, serviceDeleteController);
router.patch("/service/:id", authMiddleware, uploader.multer().any(), serviceUpdateController);

router.post(
	"/gallery",
	uploader.multer(galleryInputs).any(),
	createValidation(galleryInputs),
	galleryCreateController
);
router.get("/gallery", galleryGetListController);
router.get("/gallery/:id", galleryGetByIdController);
router.delete("/gallery/:id", galleryDeleteController);
router.patch("/gallery/:id", uploader.multer().any(), galleryUpdateController);

router.post(
	"/video",
	uploader.multer(videoInputs).any(),
	createValidation(videoInputs),
	videoCreateController
);
router.get("/video", videoGetListController);
router.get("/video/:id", videoGetByIdController);
router.delete("/video/:id", videoDeleteController);
router.patch("/video/:id", uploader.multer().any(), videoUpdateController);

router.post("/subscribe", createValidation(subscribeInputs), subscribeCreateController);
router.get("/subscribe", subscribeGetListController);
router.delete("/subscribe/:id", subscribeDeleteController);

router.post("/auth/signin", createValidation(signInInputs), signInController);
router.post("/auth/signup", createValidation(signUpInputs), signUpController);
router.get("/auth/signout", authMiddleware, signOutController);
router.patch("/auth/token/refresh", tokenController);
router.get("/auth/getme", authMiddleware, getMeController);
router.patch(
	"/auth/update/profile",
	authMiddleware,
	uploader.multer().any(),
	updateProfileController
);
router.patch(
	"/auth/update/password",
	authMiddleware,
	createValidation(passwordInputs),
	updatePasswordController
);

router.get("*", notFoundController);

export default router;
