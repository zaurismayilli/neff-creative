import { Request, Response } from "express";

import { NewSuccess, NewError } from "../../utils/error";
import { IRequest } from "../../models/request";
import { ObjectId } from "mongodb";

export const getListController = async (req: IRequest, res: Response) => {
	const { limit, page } = req.query;
	try {
		const count = await req.store.blog.getList({ limit: 0, page: 1 });
		const data = await req.store.blog.getList({ limit, page });
		const response = {
			blog: data,
			count: count.length,
		};

		return NewSuccess({ data: response, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};

export const getByIdController = async (req: IRequest, res: Response) => {
	try {
		const { id } = req.params;
		const o_id = new ObjectId(id);
		const data = await req.store.blog.getById({ id: o_id });

		return NewSuccess({ data, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
