import { createController } from "./_create";
import { updateController } from "./_update";
import { deleteController } from "./_delete";
import { getByIdController, getListController } from "./_read";

export {
	createController,
	updateController,
	deleteController,
	getByIdController,
	getListController,
};
