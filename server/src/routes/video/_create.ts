import { Response } from "express";

import { NewError, NewSuccess } from "../../utils/error";
import { IRequest } from "../../models/request";
import uploader from "../../utils/uploader";

export const createController = async (req: IRequest, res: Response) => {
	try {
		const { body } = req;
		const coverFile = req.files[0];

		const data = {
			...body,
			cover: coverFile && uploader.read(coverFile).image,
			created_at: new Date().getTime(),
		};

		await req.store.video.create(data);

		return NewSuccess({ data, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
