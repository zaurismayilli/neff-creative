import { Response } from "express";
import { ObjectId } from "mongodb";

import { IRequest } from "../../models/request";
import { NewError, NewSuccess } from "../../utils/error";
import uploader from "../../utils/uploader";

export const deleteController = async (req: IRequest, res: Response) => {
	try {
		const { id } = req.params;
		const o_id = new ObjectId(id);

		const video = await req.store.video.getById({ id: o_id });

		await req.store.video.delete({ id: o_id });
		await uploader.delete(video.image);

		const response = {
			id: o_id,
			deleted: true,
		};

		return NewSuccess({ data: response, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};