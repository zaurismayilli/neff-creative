import { Response } from "express";
import { ObjectId } from "mongodb";
import _ from "lodash";

import { NewError, NewSuccess } from "../../utils/error";
import { IRequest } from "../../models/request";
import { ERROR } from "../../constants/error";
import uploader from "../../utils/uploader";

export const updateController = async (req: IRequest, res: Response) => {
	try {
		const { body } = req;
		const { id } = req.params;
		const o_id = new ObjectId(id);

		const file = req.files && req.files[0];

		const { cover } = await req.store.video.getById({ id: o_id });

		if (_.isEmpty(cover))
			return NewError({ data: `image ${JSON.stringify(ERROR.COMMON_ERROR.NOT_FOUND)}`, res });

		let newImage;

		const data = { ...body };

		if (file) {
			newImage = uploader.update(cover, file);
			data.cover = newImage;
		}

		await req.store.video.update({ id: o_id, data });

		const response = {
			data,
			updated: true,
		};

		return NewSuccess({ data: response, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
