import { createController } from "./_create";
import { getListController, getByIdController } from "./_read";
import { deleteController } from "./_delete";
import { updateController } from "./_update";

export {
	createController,
	getListController,
	deleteController,
	updateController,
	getByIdController,
};
