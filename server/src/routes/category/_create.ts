import { Response } from "express";

import { NewError, NewSuccess } from "../../utils/error";
import { IRequest } from "../../models/request";

export const createController = async (req: IRequest, res: Response) => {
	try {
		const { body } = req;

		await req.store.category.create(body);

		return NewSuccess({ data: body, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
