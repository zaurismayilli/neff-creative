import { Response } from "express";
import { ObjectId } from "mongodb";

import { IRequest } from "../../models/request";
import { NewError, NewSuccess } from "../../utils/error";

export const updateController = async (req: IRequest, res: Response) => {
	try {
		const { body } = req;
		const { id } = req.params;
		const o_id = new ObjectId(id);

		await req.store.category.update({ id: o_id, data: body });

		const response = {
			data: body,
			updated: true,
		};

		return NewSuccess({ data: response, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
