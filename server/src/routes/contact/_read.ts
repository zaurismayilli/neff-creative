import { Response } from "express";

import { NewSuccess, NewError } from "../../utils/error";
import { IRequest } from "../../models/request";

export const getController = async (req: IRequest, res: Response) => {
	try {
		const data = await req.store.contact.get();

		return NewSuccess({ data, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
