import { Response } from "express";

import { NewError, NewSuccess } from "../../utils/error";
import { IRequest } from "../../models/request";
import uploader from "../../utils/uploader";

export const createController = async (req: IRequest, res: Response) => {
	try {
		const { body } = req;
		const file = req.files[0];

		const data = {
			...body,
			image: file && uploader.read(file).image,
			created_at: new Date().getTime(),
		};

		await req.store.service.create(data);

		return NewSuccess({ data, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
