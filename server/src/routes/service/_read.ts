import { Response } from "express";

import { NewSuccess, NewError } from "../../utils/error";
import { IRequest } from "../../models/request";
import { ObjectId } from "mongodb";

export const getListController = async (req: IRequest, res: Response) => {
	try {
		const data = await req.store.service.getList();

		return NewSuccess({ data, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};

export const getByIdController = async (req: IRequest, res: Response) => {
	try {
		const { id } = req.params;
		const o_id = new ObjectId(id);
		const data = await req.store.service.getById({ id: o_id });

		return NewSuccess({ data, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
