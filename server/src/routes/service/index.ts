import { createController } from "./_create";
import { updateController } from "./_update";
import { deleteController } from "./_delete";
import { getListController, getByIdController } from "./_read";

export {
	createController,
	updateController,
	deleteController,
	getListController,
	getByIdController,
};
