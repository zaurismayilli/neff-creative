import { Response } from "express";

import { IRequest } from "../../models/request";
import { NewError, NewSuccess } from "../../utils/error";
import { ObjectId } from "mongodb";
import uploader from "../../utils/uploader";

export const deleteController = async (req: IRequest, res: Response) => {
	try {
		const { id } = req.params;
		const o_id = new ObjectId(id);

		const service = await req.store.service.getById({ id: o_id });

		await req.store.service.delete({ id: o_id });
		await uploader.delete(service.image);

		const response = {
			id: o_id,
			deleted: true,
		};

		return NewSuccess({ data: response, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
