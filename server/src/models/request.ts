import { Request } from "express";

import BlogStore from "../db/blog";
import CategoryStore from "../db/category";
import ContactStore from "../db/contact";
import AboutStore from "../db/about";
import ServiceStore from "../db/service";
import GalleryStore from "../db/gallery";
import VideoStore from "../db/video";
import AuthStore from "../db/auth";
import { IFile } from "../utils/uploader";

export interface IRequest extends Request {
	store?: IStore | any;
	files?: IFile[] | any;
}

export interface IStore {
	blog: BlogStore;
	category: CategoryStore;
	contact: ContactStore;
	about: AboutStore;
	service: ServiceStore;
	gallery: GalleryStore;
	video: VideoStore;
	auth: AuthStore;
}
