export enum EInputType {
	DATE = "date",
	TEXT = "text",
	TEXTAREA = "textarea",
	FILE = "file",
	EMAIL = "email",
	PASSWORD = "password",
}

export interface IInputItem {
	label: string;
	id: string;
	required: boolean;
	placeholder: string;
	type: EInputType;
}
