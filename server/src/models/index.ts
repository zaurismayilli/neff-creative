export interface IPageLimit {
	limit: number;
	page: number;
}

export enum ETokenType {
	ACCESS = 'ACCESS_TOKEN',
	REFRESH = 'REFRESH_TOKEN',
}