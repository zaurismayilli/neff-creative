import Server from "./index";
import apiRouter from "../routes";
import dbClient from "../db/index";
import { store } from "./store";
import { ELoggerLevel, logger } from "../utils/logger";
import { ERROR } from "../constants/error";

class AppServer extends Server {
	constructor(port: string | number) {
		super();
		this.PORT = port;
		this.init();
		this.getReady();
	}

	private async getReady() {
		await this.connectDB();
		await this.createRoutes();
	}

	private async connectDB(): Promise<void> {
		try {
			const { DBUSER, DBPASS, DBNAME } = process.env;

			const db = await dbClient({ user: DBUSER ?? "", pass: DBPASS ?? "", name: DBNAME ?? "" });
			db.connect();
			store(this.app, db);
			logger({ message: ERROR.DATABASE.CONNECTED.message, level: ELoggerLevel.INFO });
			console.log(ERROR.DATABASE.CONNECTED.message);
		} catch (err) {
			logger({ message: ERROR.DATABASE.DISCONNECTED.message });
			console.log(ERROR.DATABASE.DISCONNECTED.message, err);
		}
	}

	private async createRoutes(): Promise<void> {
		this.app.use("/api", apiRouter);
	}
}

export default AppServer;
