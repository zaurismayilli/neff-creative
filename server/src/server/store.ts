import { MongoClient } from "mongodb";

import BlogStore from "../db/blog";
import CategoryStore from "../db/category";
import storeMiddleware from "../middlewares/store";
import ContactStore from "../db/contact";
import AboutStore from "../db/about";
import ServiceStore from "../db/service";
import GalleryStore from "../db/gallery";
import VideoStore from "../db/video";
import AuthStore from "../db/auth";
import SubscribeStore from "../db/subscribe";

export const store = async (app: any, db: MongoClient) => {
	const blogStore = new BlogStore(db);
	const categoryStore = new CategoryStore(db);
	const contactStore = new ContactStore(db);
	const aboutStore = new AboutStore(db);
	const serviceStore = new ServiceStore(db);
	const galleryStore = new GalleryStore(db);
	const videoStore = new VideoStore(db);
	const authStore = new AuthStore(db);
	const subscribeStore = new SubscribeStore(db);

	const stores = {
		blog: blogStore,
		category: categoryStore,
		contact: contactStore,
		about: aboutStore,
		service: serviceStore,
		gallery: galleryStore,
		video: videoStore,
		auth: authStore,
		subscribe: subscribeStore,
	};

	return app.use(storeMiddleware(stores));
};
