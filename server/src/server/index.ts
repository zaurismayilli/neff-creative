import cors from "cors";
import * as dotenv from "dotenv";
import path from "path";
import express, { Application, NextFunction, Request, Response, application } from "express";
import { createServer, Server as HTTPServer } from "http";

import { logger } from "../utils/logger";
import { ERROR } from "../constants/error";
import { ALLOWED_ORIGINS } from "../constants";

class Server {
	public env = {};
	public app: Application = application;
	private readonly DEFAULT_PORT: number = 8081;
	public PORT: string | number = this.DEFAULT_PORT;
	private httpServer: HTTPServer = createServer({});

	public init(): void {
		this.app = express();
		this.env = dotenv.config({ path: path.resolve(__dirname, "../.env") });
		this.httpServer = createServer(this.app);
		this.configure();
	}

	private configure(): void {
		this.app.use(cors({ origin: ALLOWED_ORIGINS }));
		this.app.use(express.static(path.join(__dirname, "../../public")));
		this.app.use(express.urlencoded({ extended: true }));
		this.app.use(express.json());

		this.app.use((err: any, req: Request, res: Response, next: NextFunction): void => {
			console.log(ERROR.COMMON_ERROR.UNCAUCHTED.message, err);
			logger({ message: ERROR.COMMON_ERROR.UNCAUCHTED.message });
			next();
		});
	}

	public start(callback: (port: string | number) => void): void {
		const PORT: string | number = this.PORT || this.DEFAULT_PORT;
		this.httpServer.listen(PORT, () => callback(PORT));
	}
}

export default Server;
