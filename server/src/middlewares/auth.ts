import { NextFunction, Response } from "express";
import _ from "lodash";

import { IRequest } from "../models/request";
import { NewError } from "../utils/error";
import { userIdIdParsedByToken } from "../utils/token";
import { ERROR } from "../constants/error";

export const authMiddleware = async (req: IRequest, res: Response, next: NextFunction) => {
	const accessToken: any = req.headers.authorization?.split("Bearer ")[1];
	const refreshToken: any = req.headers["x-refresh-token"];

	if (_.isEmpty(accessToken) || _.isEmpty(refreshToken))
		return NewError({
			code: 401,
			data: ERROR.AUTHENTICATION.TOKEN.INVALID_ACCESS_TOKEN,
			res,
		});

	const userIdFromAccessToken = await userIdIdParsedByToken({ token: accessToken });

	const userFromAccessToken = await req.store.auth.getUserByData({ userId: userIdFromAccessToken });

	const userIdFromRefreshToken = await userIdIdParsedByToken({ token: refreshToken });

	const userFromRefreshToken = await req.store.auth.getUserByData({
		userId: userIdFromRefreshToken,
	});

	if (!userIdFromAccessToken || !userFromAccessToken)
		return NewError({
			data: ERROR.AUTHENTICATION.TOKEN.INVALID_ACCESS_TOKEN,
			res,
			code: 406,
		});

	if (!userIdFromRefreshToken || !userFromRefreshToken)
		return NewError({
			data: ERROR.AUTHENTICATION.TOKEN.INVALID_REFRESH_TOKEN,
			res,
			code: 403,
		});

	if (
		userFromAccessToken.accessToken !== accessToken ||
		userFromRefreshToken.refreshToken !== refreshToken
	)
		return NewError({
			data: ERROR.AUTHENTICATION.TOKEN.DO_NOT_MATCH_TOKEN,
			res,
			code: 403,
		});

	next();
};
