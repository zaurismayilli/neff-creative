import { NextFunction, Response } from "express";

import { IRequest, IStore } from "../models/request";

const storeMiddleware =
	(stores: IStore) =>
	(req: IRequest, res: Response, next: NextFunction): void => {
		if (stores !== null) req.store = stores;
		next();
	};

export default storeMiddleware;
