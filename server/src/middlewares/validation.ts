import { Request, Response, NextFunction } from "express";

import { IInputItem } from "../models/form";
import { NewError } from "../utils/error";
import { ERROR } from "../constants/error";
import { checkValidation } from "../utils/validation";

export const createValidation =
	(inputs: IInputItem[]) => async (req: Request, res: Response, next: NextFunction) => {
		try {
			const { body } = req;

			const { isValid, filteredInputs } = checkValidation(inputs)(body);

			if (!isValid) {
				return NewError({ data: ERROR.COMMON_ERROR.MISSING_FIELDS(filteredInputs), res });
			}
			next();
		} catch (err) {
			return NewError({ data: err, res });
		}
	};
