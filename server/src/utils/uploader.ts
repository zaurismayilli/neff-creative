import multer from "multer";
import path from "path";
import fs from "fs";

import { checkValidation } from "./validation";
import { IInputItem } from "../models/form";
import { logger } from "./logger";
import { ERROR } from "../constants/error";

export interface IImage {
	path: string;
	filename: string;
	directory?: string;
}

export interface IFile extends IImage {
	fieldname: string;
	originalname: string;
	encoding: string;
	size: number;
	mimetype: string;
	destination: string;
}

interface IUpload {
	read(file: IFile): {
		image: {};
		originalDirectory: string;
	};
	create(inputs?: IInputItem[] | undefined): any;
	multer(inputs?: IInputItem[] | undefined): any;
	delete(file: IFile): void;
	update(oldFIle: IFile, newFile: IFile): void;
}

const uploader: IUpload = {
	read: (file: IFile) => {
		const folderName = file.fieldname?.split("_")[0];
		const directory = file.directory ?? `/uploads/${folderName}/`;
		const originalDirectory = __dirname + `/../../public/${directory}`;

		const filename = file?.filename;
		const imagePath = directory + filename;

		const image = {
			filename,
			directory,
			path: imagePath,
		};

		return {
			originalDirectory,
			image,
		};
	},
	create(inputs) {
		try {
			return multer.diskStorage({
				destination: async (req, file, cb) => {
					const { isValid } = await checkValidation(inputs)(req.body);

					if (!isValid) logger({ message: ERROR.COMMON_ERROR.MISSING_FIELDS(inputs).message });

					const { originalDirectory } = this.read(file);
					if (!fs.existsSync(originalDirectory)) {
						fs.mkdirSync(originalDirectory, { recursive: true });
					}
					cb(null, originalDirectory);
				},
				filename: (req, file, cb) => {
					cb(null, Date.now() + path.extname(file.originalname));
				},
			});
		} catch (err: any) {
			return logger({ message: err.toString() });
		}
	},
	delete(file: IFile) {
		try {
			return fs.unlink(this.read(file).originalDirectory + file.filename, function (err) {
				if (err) logger({ message: err.toString() });
			});
		} catch (err: any) {
			return logger({ message: err.toString() });
		}
	},
	update(oldFile: IFile, newFile: IFile) {
		try {
			this.delete(oldFile);
			return this.read(newFile).image;
		} catch (err: any) {
			return logger({ message: err.toString() });
		}
	},
	multer(inputs) {
		try {
			return multer({ storage: this?.create(inputs || []), dest: "uploads" });
		} catch (err: any) {
			return logger({ message: err.toString() });
		}
	},
};

export default uploader;
