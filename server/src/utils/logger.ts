import winston, { LoggerOptions, createLogger, format, transports } from "winston";

export enum ELoggerLevel {
	ERROR = "error",
	WARN = "warn",
	INFO = "info",
	HTTP = "http",
	VERBOSE = "verbose",
	DEBUG = "debug",
	SILLY = "silly",
}

export enum ELoggerFilename {}

interface ICustomerLogger {
	filename?: string;
	level?: ELoggerLevel;
	message: string;
}

export const logger = ({ message, level = ELoggerLevel.ERROR }: ICustomerLogger) =>
	createLogger({
		transports: [
			new transports.File({
				filename: `public/log/app-${level}.log`,
				level,
				format: format.combine(format.timestamp(), format.json()),
			}),
		],
	}).log(level, message);
