import jwt from "jsonwebtoken";
import { ObjectId } from "mongodb";

import { logger } from "./logger";

export enum ETokenType {
	ACCESS = "ACCESS_TOKEN",
	REFRESH = "REFRESH_TOKEN",
}

interface IToken {
	payload: any;
	time?: number;
	secret?: string | any;
	type?: ETokenType;
}

export const generateToken = async ({
	payload,
	time = 15,
	type = ETokenType.ACCESS,
	secret = process.env.JWT_SECRET,
}: IToken) => {
	try {
		return await jwt.sign(payload, secret, {
			algorithm: "HS256",
			expiresIn: time * 60,
		});
	} catch (err: any) {
		logger({ message: err.toString() });
		return err;
	}
};

export const verifyToken = async (token: string, secret: any = process.env.JWT_SECRET) => {
	try {
		return jwt.verify(token, secret);
	} catch (err: any) {
		logger({ message: err.toString() });
		return err;
	}
};

export const generateMultiToken = async ({ payload }: any) => {
	const accessToken = await generateToken({ payload });
	const refreshToken = await generateToken({
		payload,
		type: ETokenType.REFRESH,
		time: 3600 * 24 * 60,
	});

	return {
		accessToken,
		refreshToken,
	};
};

export const userIdIdParsedByToken = async ({ token }: { token: string }) => {
	try {
		const verify: any = await verifyToken(token);
		const { userId } = verify;
		if (!!userId) return new ObjectId(userId);
	} catch (err: any) {
		logger({ message: err.toString() });
		return err;
	}
};
