import { IInputItem } from "../models/form";

export const checkValidation =
	(inputs: IInputItem[] | null = null) =>
	(body: any) => {
		if (!inputs) return { isValid: true, filteredInputs: null };
		const filteredInputs = inputs
			.filter((input, key) => input.required && Object.keys(body)[key] !== input.id)
			.map(({ id }) => id);

		const isValid = filteredInputs.every((id) => !!body[id]);

		return {
			isValid,
			filteredInputs,
		};
	};
