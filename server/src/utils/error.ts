import { Response } from "express";

import { ELoggerLevel, logger } from "./logger";

interface IResponse {
	code?: number;
	data: any;
	res: Response;
}

export const NewError = ({ data, res, code = 400 }: IResponse) => {
	console.log(data, code, "-----------ERROR DATA-----------");
	logger({ message: data.message || data });
	return res.status(code).json({
		message: data.message || data.toString(),
	});
};

export const NewSuccess = ({ code = 200, data, res }: IResponse) => {
	console.log(data, "-----------SUCCESS DATA-----------");
	logger({ message: JSON.stringify(data), level: ELoggerLevel.INFO });
	return res.status(code).json(data);
};
