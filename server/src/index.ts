import AppServer from "./server/app-server";

const app = new AppServer(1001);

app.start((port) => {
	console.log("MESSAGE SERVER IS LISTENING", port);
});
