import { IInputItem } from "../models/form";

export const COMMON_ERROR = {};

export const ERROR = {
	COMMON_ERROR: {
		MISSING_FIELDS: (inputs: IInputItem[] | Array<string> | null | undefined) => {
			console.log(inputs);
			return {
				message: `Please fill these missing fields: ${JSON.stringify(inputs)}`,
			};
		},
		UNCAUCHTED: {
			message: "Uncatched error",
		},
		NOT_FOUND: {
			message: "not found",
		},
	},
	DATABASE: {
		CONNECTED: {
			message: "CONNECTED TO DB",
		},
		DISCONNECTED: {
			message: "ERROR ON DB CONNECTION",
		},
	},
	AUTHENTICATION: {
		TOKEN: {
			INVALID_ACCESS_TOKEN: {
				message: "please generate access token",
			},
			INVALID_REFRESH_TOKEN: {
				message: "please generate refresh token",
			},
			DO_NOT_MATCH_TOKEN: {
				message: "there is no active token",
			},
			EXPIRE_REFRESH_TOKEN: {
				message: "your refresh token is expired",
			},
		},
		VALIDATION: {
			USER_ALREADY_EXIST: {
				message: "User Already Exist",
			},
			USER_NOT_FOUND: {
				message: "User tapilmadi",
			},
			INCORRECT_ACCOUNT: {
				message: "Incorrect email",
			},
			INCORRECT_PASSWORD: {
				message: "Incorrect password",
			},
			DO_NOT_EXIST_USER: {
				message: "DO not exist user",
			},
			DO_NOT_MATCH_PASSWORD: {
				message: "Password do not match",
			},
			SIGN_OUT: {
				message: "Successfully logged out"
			}
		},
	},
};
