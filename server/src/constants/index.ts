export const ALLOWED_ORIGINS = [
	"http://localhost:3000",
	"http://127.0.0.1:5500",
	"http://localhost:5500",
	"http://localhost:1000",
	"http://localhost:1002",
];
