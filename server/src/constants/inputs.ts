import { EInputType, IInputItem } from "../models/form";

export const blogInputs: IInputItem[] = [
	{
		label: "Blog Title",
		required: true,
		placeholder: "Blog Title",
		id: "title",
		type: EInputType.TEXT,
	},
	{
		label: "Blog Author",
		required: true,
		placeholder: "Blog Author",
		id: "author",
		type: EInputType.TEXT,
	},
	// {
	// 	label: "Blog Image",
	// 	required: false,
	// 	placeholder: "Blog Image",
	// 	id: "blog_image",
	// 	type: EInputType.FILE,
	// },
	{
		label: "Blog Content",
		required: true,
		placeholder: "Blog Content",
		id: "content",
		type: EInputType.TEXTAREA,
	},
	{
		label: "Blog Category",
		required: true,
		placeholder: "Blog Category",
		id: "categoryId",
		type: EInputType.TEXT,
	},
];

export const categoryInputs: IInputItem[] = [
	{
		label: "Category Name",
		required: true,
		placeholder: "Category Name",
		id: "category_name",
		type: EInputType.TEXT,
	},
];

export const serviceInputs: IInputItem[] = [
	{
		label: "Service Title",
		required: true,
		placeholder: "Service Title",
		id: "title",
		type: EInputType.TEXT,
	},
	// {
	// 	label: "Service Image",
	// 	required: true,
	// 	placeholder: "Service Image",
	// 	id: "image",
	// 	type: EInputType.TEXT,
	// },
];

export const galleryInputs: IInputItem[] = [
	{
		label: "Gallery Title",
		required: true,
		placeholder: "Gallery Title",
		id: "title",
		type: EInputType.TEXT,
	},
	// {
	// 	label: "Gallery Image",
	// 	required: true,
	// 	placeholder: "Gallery Image",
	// 	id: "image",
	// 	type: EInputType.TEXT,
	// },
	{
		label: "Gallery Category",
		required: true,
		placeholder: "Gallery Category",
		id: "categoryId",
		type: EInputType.TEXT,
	},
];

export const videoInputs: IInputItem[] = [
	{
		label: "Video Title",
		required: true,
		placeholder: "Video Title",
		id: "title",
		type: EInputType.TEXT,
	},
	{
		label: "Video Author",
		required: true,
		placeholder: "Video Author",
		id: "author",
		type: EInputType.TEXT,
	},
	// {
	// 	label: "Video Image",
	// 	required: true,
	// 	placeholder: "Video Image",
	// 	id: "image",
	// 	type: EInputType.FILE,
	// },
	{
		label: "Video Description",
		required: true,
		placeholder: "Video Description",
		id: "description",
		type: EInputType.TEXTAREA,
	},
	{
		label: "Video Category",
		required: true,
		placeholder: "Video Category",
		id: "categoryId",
		type: EInputType.TEXT,
	},
];

export const signInInputs: IInputItem[] = [
	{
		label: "SIgn in email",
		required: true,
		placeholder: "SIgn in email",
		id: "email",
		type: EInputType.EMAIL,
	},
	{
		label: "Sign in password",
		required: true,
		placeholder: "Sign in password",
		id: "password",
		type: EInputType.PASSWORD,
	},
];

export const signUpInputs: IInputItem[] = [
	{
		label: "Sign up firstname",
		required: true,
		placeholder: "Sign up firstname",
		id: "firstname",
		type: EInputType.TEXT,
	},
	{
		label: "Sign up lastname",
		required: true,
		placeholder: "Sign up lastname",
		id: "lastname",
		type: EInputType.TEXT,
	},
	{
		label: "Sign up email",
		required: true,
		placeholder: "Sign up email",
		id: "email",
		type: EInputType.EMAIL,
	},
	{
		label: "Sign up password",
		required: true,
		placeholder: "Sign up password",
		id: "password",
		type: EInputType.PASSWORD,
	},
	{
		label: "Sign up password repeat",
		required: true,
		placeholder: "Sign up password repeat",
		id: "password_repeat",
		type: EInputType.PASSWORD,
	},
];

export const profileInputs: IInputItem[] = [
	{
		label: "firstname",
		required: false,
		placeholder: "firstname",
		id: "firstname",
		type: EInputType.TEXT,
	},
	{
		label: "lastname",
		required: false,
		placeholder: "lastname",
		id: "lastname",
		type: EInputType.TEXT,
	},
];

export const passwordInputs: IInputItem[] = [
	{
		label: "Old password",
		required: true,
		placeholder: "Old password",
		id: "password_old",
		type: EInputType.PASSWORD,
	},
	{
		label: "New password",
		required: true,
		placeholder: "New password",
		id: "password_new",
		type: EInputType.PASSWORD,
	},
	{
		label: "New password repeat",
		required: false,
		placeholder: "New password repeat",
		id: "password_new_repeat",
		type: EInputType.PASSWORD,
	},
];

export const subscribeInputs: IInputItem[] = [
	{
		label: "Subscribe Name",
		required: true,
		placeholder: "Subscribe Name",
		id: "name",
		type: EInputType.TEXT,
	},
	{
		label: "Subscribe Email",
		required: true,
		placeholder: "Subscribe Email",
		id: "email",
		type: EInputType.EMAIL,
	},
	{
		label: "Subscribe Subject",
		required: true,
		placeholder: "Subscribe Subject",
		id: "subject",
		type: EInputType.TEXT,
	},
	{
		label: "Subscribe Text",
		required: true,
		placeholder: "Subscribe Text",
		id: "text",
		type: EInputType.TEXTAREA,
	},
];
