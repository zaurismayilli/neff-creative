import { Collection, MongoClient } from "mongodb";

import { COLLECTIONS } from "./const";

interface ISubscribeData {
	name: string;
	email: string;
	subject: string;
	text: string;
}

class SubscribeStore {
	private collection: Collection<any>;

	constructor(client: MongoClient) {
		this.collection = client.db().collection(COLLECTIONS.SUBSCRIBE);
	}

	async create(data: ISubscribeData) {
		// return	this.collection.deleteMany({})
		return await this.collection.insertOne(data);
	}

	async delete({ id }: { id: string }) {
		return await this.collection.deleteOne({ _id: id });
	}

	async getList({ limit = 5, page = 1 } = {}) {
		limit = +limit;
		page = +page;
		return await this.collection
			.find({})
			.sort({ created_at: 1 })
			.skip(page * limit - limit)
			.limit(limit)
			.toArray();
	}
}

export default SubscribeStore;
