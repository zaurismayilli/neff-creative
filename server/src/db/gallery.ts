import { Collection, MongoClient } from "mongodb";

import { COLLECTIONS } from "./const";
import { IPageLimit } from "../models";

interface IGalleryData {
	title?: string;
	image?: string;
	categoryId?: string;
	created_at?: string;
}

interface IGalleryList extends IPageLimit {
	categoryId: string;
}

class GalleryStore {
	private collection: Collection<any>;

	constructor(client: MongoClient) {
		this.collection = client.db().collection(COLLECTIONS.GALLERY);
	}

	async create(data: IGalleryData) {
		//return	this.collection.deleteMany({})
		return await this.collection.insertOne(data);
	}

	async update({ id, data }: { id: string; data: IGalleryData }) {
		return await this.collection.updateOne({ _id: id }, { $set: data });
	}

	async getById({ id }: { id: string }) {
		return await this.collection.findOne({ _id: id });
	}

	async delete({ id }: { id: string }) {
		return await this.collection.deleteOne({ _id: id });
	}

	async getList({ limit = 5, page = 1 } = {}) {
		limit = +limit;
		page = +page;
		return await this.collection
			.find({})
			.sort({ created_at: 1 })
			.skip(page * limit - limit)
			.limit(limit)
			.toArray();
	}

	async getListByCategoryId({ categoryId, limit = 5, page = 1 }: IGalleryList) {
		limit = +limit;
		page = +page;
		return await this.collection
			.find({ categoryId })
			.sort({ created_at: 1 })
			.skip(page * limit - limit)
			.limit(limit)
			.toArray();
	}
}

export default GalleryStore;
