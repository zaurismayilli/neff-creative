import { Collection, MongoClient } from "mongodb";

import { COLLECTIONS } from "./const";

interface IAboutData {
	title?: string;
	description?: string;
	content?: string;
}

class AboutStore {
	private collection: Collection<any>;

	constructor(client: MongoClient) {
		this.collection = client.db().collection(COLLECTIONS.ABOUT);
	}

	async update(data: IAboutData) {
		return await this.collection.updateOne({}, { $set: data });
	}

	async get() {
		return await this.collection.findOne({});
	}
}

export default AboutStore;
