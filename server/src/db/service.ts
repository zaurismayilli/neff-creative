import { Collection, MongoClient } from "mongodb";

import { COLLECTIONS } from "./const";

interface IServiceData {
	title?: string;
	image?: string;
}

class ServiceStore {
	private collection: Collection<any>;

	constructor(client: MongoClient) {
		this.collection = client.db().collection(COLLECTIONS.SERVICE);
	}

	async create(data: IServiceData) {
		//return	this.collection.deleteMany({})
		return await this.collection.insertOne(data);
	}

	async update({ id, data }: { id: string; data: IServiceData }) {
		return await this.collection.updateOne({ _id: id }, { $set: data });
	}

	async getById({ id }: { id: string }) {
		return await this.collection.findOne({ _id: id });
	}

	async delete({ id }: { id: string }) {
		return await this.collection.deleteOne({ _id: id });
	}

	async getList() {
		return await this.collection.find({}).sort({ created_at: 1 }).toArray();
	}
}

export default ServiceStore;
