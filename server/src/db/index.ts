import { MongoClient, ServerApiVersion } from "mongodb";

interface IDatabase {
	user: string;
	pass: string;
	name: string;
}

const client = ({ user, pass, name }: IDatabase) => {
	const url = `mongodb+srv://${user}:${pass}@cluster0.tdtsvua.mongodb.net/${name}?retryWrites=true&w=majority`;

	return new MongoClient(url, {
		serverApi: {
			version: ServerApiVersion.v1,
			strict: true,
			deprecationErrors: true,
		},
	});
};

export default client;
