import { Collection, MongoClient } from "mongodb";

import { COLLECTIONS } from "./const";

interface ICategoryData {
	category_name?: string;
}

class CategoryStore {
	private collection: Collection<any>;

	constructor(client: MongoClient) {
		this.collection = client.db().collection(COLLECTIONS.CATEGORY);
	}

	async create(data: ICategoryData) {
		return await this.collection.insertOne(data);
	}

	async update({ id, data }: { id: string; data: ICategoryData }) {
		return await this.collection.updateOne({ _id: id }, { $set: data });
	}

	async getById({ id }: { id: string }) {
		return await this.collection.findOne({ _id: id });
	}

	async delete({ id }: { id: string }) {
		return await this.collection.deleteOne({ _id: id });
	}

	async getList() {
		return await this.collection.find({}).toArray();
	}
}

export default CategoryStore;
