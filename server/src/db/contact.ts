import { Collection, MongoClient } from "mongodb";

import { COLLECTIONS } from "./const";

interface IContactData {
	title?: string;
	address?: string;
	email?: string;
	mobile?: string;
	content?: string;
	position?: {
		latitude: number,
		longitude: number
	};
	description?: string;
}

class ContactStore {
	private collection: Collection<any>;

	constructor(client: MongoClient) {
		this.collection = client.db().collection(COLLECTIONS.CONTACT);
	}

	async update(data: IContactData) {
		return await this.collection.updateOne({}, { $set: data });
	}

	async get() {
		return await this.collection.findOne({})
	}
}

export default ContactStore;