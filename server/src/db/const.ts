export const COLLECTIONS = {
	BLOG: "blog",
	CATEGORY: "category",
	CONTACT: "contact",
	ABOUT: "about",
	SERVICE: "service",
	GALLERY: "gallery",
	VIDEO: "video",
	AUTH: "auth",
	SUBSCRIBE: "subscribe",
};
