import { Collection, MongoClient } from "mongodb";

import { COLLECTIONS } from "./const";

interface IBlogData {
	title?: string;
	author?: string;
	content?: string;
	categoryId: string;
}

class BlogStore {
	private collection: Collection<any>;

	constructor(client: MongoClient) {
		this.collection = client.db().collection(COLLECTIONS.BLOG);
	}

	async create(data: IBlogData) {
		// return	this.collection.deleteMany({})
		return await this.collection.insertOne(data);
	}

	async update({ id, data }: { id: string; data: IBlogData }) {
		return await this.collection.updateOne({ _id: id }, { $set: data });
	}

	async getById({ id }: { id: string }) {
		return await this.collection.findOne({ _id: id });
	}

	async delete({ id }: { id: string }) {
		return await this.collection.deleteOne({ _id: id });
	}

	async getList({ limit = 5, page = 1 } = {}) {
		limit = +limit;
		page = +page;
		return await this.collection
			.find({})
			.sort({ created_at: 1 })
			.skip(page * limit - limit)
			.limit(limit)
			.toArray();
	}
}

export default BlogStore;
