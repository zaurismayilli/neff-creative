import { Collection, MongoClient } from "mongodb";

import { COLLECTIONS } from "./const";
import { IFile } from "../utils/uploader";

interface IAuthData {
	firstname?: string;
	lastname?: string;
	email?: string;
	password?: string;
	image?: IFile;
}

class AuthStore {
	private collection: Collection<any>;

	constructor(client: MongoClient) {
		this.collection = client.db().collection(COLLECTIONS.AUTH);
	}

	async signUp(data: IAuthData) {
		return await this.collection.insertOne(data);
	}

	async signOut({ userId }: { userId: string }) {
		return await this.collection.updateOne(
			{ userId },
			{ $set: { accessToken: null, refreshToken: null } }
		);
	}

	async update({ userId, data }: { userId: string; data: IAuthData }) {
		return await this.collection.updateOne({ userId }, { $set: data });
	}

	async getUserByData(data: IAuthData) {
		// await this.collection.deleteMany({});
		return await this.collection.findOne(data);
	}
}

export default AuthStore;
